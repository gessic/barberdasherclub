import React, { Component } from 'react';
import { StyleSheet, Text,Image, View,TouchableOpacity} from 'react-native';

class NavigationDrawer extends Component {
    constructor(props) {
        super(props);
        this.state = {  };
    }
    render() {
        return (
            <View style={styles.drawer}>
                <View style={{justifyContent: 'center', alignItems:'center',}}>
                    <TouchableOpacity style={styles.profilePic}  >
                    <Text> Add Profile Pic </Text>
                    </TouchableOpacity>
                    <Text> Abdul Osman </Text>
                </View>
                <View style={{padding: 20 }}>
                    <TouchableOpacity style={styles.item}>
                    <Text> Services </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.item}>
                    <Text> History </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.item}>
                    <Text> Products </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.item}>
                    <Text> Grooming Tips </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.item}>
                    <Text> Rewards </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.item}>
                    <Text> Consierge </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.item}>
                    <Text> Settings </Text>
                    </TouchableOpacity>

                </View>

            </View>
        );
    }
}

export default NavigationDrawer;

const styles = StyleSheet.create(
    {
        drawer : {
            width : 200,
            height : 1000,
            borderWidth: 1,
        },
        profilePic: {
            borderWidth : 1,
            borderColor: '#212121',
            borderRadius:150,
            height: 150,
            width: 150,
            justifyContent: 'center', 
            alignItems:'center',
            margin: 10,
            marginTop: 50,
        },
        item: {
            borderBottomWidth: 1,
            paddingBottom: 20,
            paddingTop: 20,
            justifyContent: 'center', 
            alignItems:'center',
        }
    }
)