import React, { Component } from 'react';
import { StyleSheet,  View,KeyboardAvoidingView, Text, TouchableOpacity} from 'react-native';
import { Input, ButtonGroup } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';


class TipScreen extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            text: 0,
            tenPercent: 0,
            fifteenPercent: 0,
            twentyPercent: 0,
            twentyFivePercent: 0

         };
         

        this.updateIndex = this.updateIndex.bind(this)
    }

    updateIndex (selectedIndex) {
        if(selectedIndex){
            this.props.setTip(+(+this.state.text).toFixed(2))
            this.props.toggleTipScreen()
        }
        else{
            this.props.toggleTipScreen()
        }
      }

    tipAmount(){
        
        return this.state.text
    }

    percentageCalculation(){
        currentAmount = this.props.total
        if(total){
            return
        }
        else{
            this.setState({ tenPercent: currentAmount * .10 , fifteenPercent: currentAmount * .15, twentyPercent: currentAmount * .2, twentyFivePercent: currentAmount * .2 })
        }

    }

    render() {
        const tenPercent =  this.props.total * .10
        const fifteenPercent =  this.props.total * .15
        const twentyPercent =  this.props.total * .20
        const twentyFivePercent =  this.props.total * .25

        return (
            <KeyboardAvoidingView behavior="height" enabled style={styles.container}>
                <View style={styles.tipAmount}>
                    <Text style={{fontSize: 25,}}> ${this.tipAmount()} </Text>
                </View>
                <View style={styles.presetTipAmountRow}>
                    <TouchableOpacity style={styles.presetTipAmount} onPress={() => this.setState({text: +tenPercent.toFixed(2)})}>
                    <View style={{padding: 5}}>
                            <Text style={{fontSize: 20,}}> 10% </Text>
                        </View>
                        <View style={{padding: 5, justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={{fontSize: 11,}}> ${tenPercent.toFixed(2)} </Text>
                        </View>

                    </TouchableOpacity>

                    <TouchableOpacity style={styles.presetTipAmount} onPress={() => this.setState({text: +fifteenPercent.toFixed(2)})}>
                    <View style={{padding: 5}}>
                            <Text style={{fontSize: 20,}}> 15% </Text>
                        </View>

                        <View style={{padding: 5, justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={{fontSize: 11,}}> ${fifteenPercent.toFixed(2)} </Text>
                        </View>

                    </TouchableOpacity>

                </View>

                <View style={styles.presetTipAmountRow}>
                    <TouchableOpacity style={styles.presetTipAmount} onPress={() => this.setState({text: +twentyPercent.toFixed(2)})}>
                    <View style={{padding: 5,}}>
                            <Text style={{fontSize: 20,}}> 20% </Text>
                        </View>

                        <View style={{padding: 5, justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={{fontSize: 11,}}> ${twentyPercent.toFixed(2)} </Text>
                        </View>

                    </TouchableOpacity>

                    <TouchableOpacity style={styles.presetTipAmount} onPress={() => this.setState({text: +twentyFivePercent.toFixed(2)})}>
                        <View style={{padding: 5}}>
                            <Text style={{fontSize: 20,}}> 25% </Text>
                        </View>

                        <View style={{padding: 5, justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={{fontSize: 11,}}> ${twentyFivePercent.toFixed(2)} </Text>
                        </View>

                    </TouchableOpacity>
                    
                </View>

                <KeyboardAvoidingView behavior="padding" enabled style={styles.customTipInput}>
                    <Text> $ </Text>
                    <Input
                    keyboardType='number-pad'
                    returnKeyType='done'
                    inputContainerStyle={{borderBottomWidth: 0}}
                    placeholder='  Custom'
                    onChangeText={text => this.setState({text})}
                    placeholderTextColor={'#888888'}
                    style={{color: '#212121'}}
                    
                    />
                </KeyboardAvoidingView>

                <View style={styles.dismissButtons}>
                <ButtonGroup
                        onPress={this.updateIndex}
                        //selectedIndex={selectedIndex}
                        buttons={['Cancel', 'OK']}
                        containerBorderRadius={0}
                        selectedButtonStyle={{backgroundColor: 'black'}}
                        containerStyle={{height: 30, width: 200, }}
                        />
                </View>

            </KeyboardAvoidingView>
        );
    }
}
export default TipScreen;

const styles = StyleSheet.create(
    {
        container : {
            width: 300,
            height: 400,
            backgroundColor: 'white',
            
        },
        tipAmount : {
            alignSelf: 'center',
            margin: 30,
            
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 3,
            },
            shadowOpacity: 0.27,
            shadowRadius: 4.65,
            elevation: 6,
        
        },
        presetTipAmountRow: {
            
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',

        },
        presetTipAmount: {
            margin: 17, 
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 3,
            },
            shadowOpacity: 0.27,
            shadowRadius: 4.65,
            elevation: 6,


        },
        customTipInput: {
            margin: 10,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 3,
            },
            shadowOpacity: 0.27,
            shadowRadius: 4.65,
            elevation: 6,
            marginLeft: 80,
            marginRight: 80,

        },
        dismissButtons: {
            margin: 10,
            justifyContent: 'center',
            alignItems: 'center',

        }

    }
)

