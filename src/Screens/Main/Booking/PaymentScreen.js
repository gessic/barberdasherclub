import React, { Component } from 'react';
import {Platform, StyleSheet, Dimensions, Text, View,TouchableOpacity, ScrollView} from 'react-native';
import { Input } from 'react-native-elements'
import moment from "moment";
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Overlay, Card,Tooltip} from 'react-native-elements';
import firebase from 'react-native-firebase'
// import {
//     SQIPCore,
//     SQIPApplePay,
//     SQIPCardEntry,
//   } from 'react-native-square-in-app-payments';
import 'react-native-get-random-values';
// import { v4 as uuidv4 } from 'uuid';
import { ButtonGroup, Button } from 'react-native-elements';
const base64 = require('base-64');


class PaymentScreen extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            selectedIndex: 1,
            successOverlayVisible : false,
            cardError :false,
            errorOverlayVisible: false,
            user:null,
            // canUseDigitalWallet: false,
            responseJSON : {}
         };

        // this.onStartCardEntry = this.onStartCardEntry.bind(this);
        // this.onCardNonceRequestSuccess = this.onCardNonceRequestSuccess.bind(this);
        this.updateIndex = this.updateIndex.bind(this)
        // this.onStartDigitalWallet = this.onStartDigitalWallet.bind(this);
        // this.onApplePayNonceRequestSuccess = this.onApplePayNonceRequestSuccess.bind(this)
        // this.onApplePayNonceRequestFailure = this.onApplePayNonceRequestFailure.bind(this)
        // this.onApplePayEntryComplete = this.onApplePayEntryComplete.bind(this)
         
    }

    async componentDidMount() {
        // await SQIPCore.setSquareApplicationId('sandbox-sq0idb-vCpxOwQ5KByIHy1xL6Uy_w');

        // let digitalWalletEnabled = false;
        // if (Platform.OS === 'ios') {
        // await SQIPApplePay.initializeApplePay('merchant.com.gessic.barberdasherclub');
        // digitalWalletEnabled = await SQIPApplePay.canUseApplePay();
        // } 
        // this.setState({
        //     canUseDigitalWallet: digitalWalletEnabled,
        //   });
        // else if (Platform.OS === 'android') {
        
        // }
        firebase.auth().onAuthStateChanged((user) => {
          if (user) {
            this.setState({  user: user });
          } else {
            console.log('no user') 
          }
        });
      }

//         /**
//    * Callback when successfully get the card nonce details for processig
//    * apple pay sheet is still open and waiting for processing card nonce details
//    * @param cardDetails
//    */
//     async onApplePayNonceRequestSuccess(cardDetails) {
//         try {
            
//             console.log(cardDetails)
//         // take payment with the card nonce details
//         // you can take a charge
//         // await chargeCard(cardDetails);

//         // you must call completeApplePayAuthorization to close apple pay sheet
//         await SQIPApplePay.completeApplePayAuthorization(true);
//         } catch (ex) {
//         // handle card nonce processing failure

//         // you must call completeApplePayAuthorization to close apple pay sheet
//         await SQIPApplePay.completeApplePayAuthorization(false, ex.message);
//         }
//     }

//         /**
//      * Callback when failed to get the card nonce
//      * apple pay sheet is still open and waiting for processing error information
//      */
//     async onApplePayNonceRequestFailure(errorInfo) {
//         // handle this error before close the apple pay sheet

//         // you must call completeApplePayAuthorization to close apple pay sheet
//         await SQIPApplePay.completeApplePayAuthorization(false, errorInfo.message);
//     }
//     onApplePayEntryComplete() {
//         // handle the apple pay sheet closed event
//       }
    
//         /**
//    * An event listener to start digital wallet flow
//    */
//     async onStartDigitalWallet() {
//         if (Platform.OS === 'ios') {

//             const applePayConfig = {
//                 price: 10,
//                 summaryLabel: 'Test Item',
//                 countryCode: 'CA',
//                 currencyCode: 'CAD',
//                 paymentType: SQIPApplePay.PaymentTypeFinal,
//             };

//         try {

//             console.log(SQIPApplePay)
//             await SQIPApplePay.requestApplePayNonce(
//             applePayConfig,
//             this.onApplePayNonceRequestSuccess,
//             this.onApplePayNonceRequestFailure,
//             this.onApplePayEntryComplete,
//             );
//         } catch (ex) {
//             console.log("errossr", ex)
//             // Handle InAppPaymentsException
//         }
//         } 
//         // else if (Platform.OS === 'android') {
//         // ...
//         // }
//     }
//       /**
//      * Callback when the card entry is closed after call 'SQIPCardEntry.completeCardEntry'
//      */
//     onCardEntryComplete(results) {
     
//         if(results?.payment?.card_details.status === 'CAPTURED' && results?.payment?.status ==='COMPLETED'){
//             this.createBooking()
//         }
//         else{
//             console.log('error')
//             this.setState({cardError: true, errorOverlayVisible: true})        }
//         // Update UI to notify user that the payment flow is completed
//     }

//      /**
//    * Callback when successfully get the card nonce details for processig
//    * card entry is still open and waiting for processing card nonce details
//    * @param {*} cardDetails
//    */
//     async onCardNonceRequestSuccess(cardDetails) {
        
//         try {
            
     
//             let headers = {
//                 method: 'POST',
//                 headers: {
//                     'Accept': 'application/json',
//                     'Content-Type': 'application/json',
//                     'Authorization': 'Bearer ' + 'EAAAEFlqJBljXBOZxSHJEr4Jd1I-m6FL1jQ33gFl2Hd2u8v9EIOWmg0-QzVWFrKf'
//                 },
//                 body: JSON.stringify({
//                     "source_id": cardDetails.nonce,
//                     "idempotency_key": uuidv4(),
//                     "amount_money" : {
//                         "amount" : this.props.price -this.props.bookingDetails.tip,
//                         "currency" : "CAD",
//                     },
//                     "tip_money" : {
//                         "amount": this.props.bookingDetails.tip ,
//                         "currency" : "CAD"
//                     },
//                     "autocomplete" : true,
//                     "statement_description": "Barber Dasher Club"
//                 })
//             }
//             let paymentLink = "https://connect.squareupsandbox.com/v2/payments"

//             let results = await  fetch(paymentLink, headers)
//             let resultsJSON = await results.json()
            
//             // fetch(paymentLink, headers)
//             // .then((response) => response.json())
//             // .then((responseJSON) => {this.setState(responseJSON); console.log(responseJSON)})
//             // .catch((error) => console.log(error))
           
//         await SQIPCardEntry.completeCardEntry(
//             this.onCardEntryComplete(resultsJSON),
//         );
//         } catch (ex) {
//         // payment failed to complete due to error
//         // notify card entry to show processing error

//         await SQIPCardEntry.showCardNonceProcessingError(ex.message);
//         }
//     }

//       /**
//      * Callback when card entry is cancelled and UI is closed
//      */
//     onCardEntryCancel() {
//         // Handle the cancel callback
//     }

//       /**
//      * An event listener to start card entry flow
//      */
//     async onStartCardEntry() {
//         try{
//             const cardEntryConfig = {
//                 collectPostalCode: true,
//                 };
//                 await SQIPCardEntry.startCardEntryFlow(
//                 cardEntryConfig,
//                 this.onCardNonceRequestSuccess,
//                 this.onCardEntryCancel,
//                 );
                
//         }
//         catch(error){
//             console.log(error)
//         }
        
//     }


    

    updateIndex (selectedIndex) {
        this.setState({selectedIndex})
      }

    getUserEmail(){
        return this.state.user.email
    }

    // showCardProcessingEror = () => {
    //     this.setState({errorOverlayVisible : true})
    // }
      createBooking = async function(){
        var myHeaders = new Headers();
        myHeaders.append('Authorization', "Basic " + base64.encode("18061059:df533abcef6b28a54a80a6e53737a935"));

        moment(this.props.bookingDetails.formattedTime, 'hhmm a').format('HHmmss')
        time = moment(this.props.bookingDetails.chosenDateObject).format("YYYY-MM-DD") + 'T' + moment(this.props.bookingDetails.formattedTime, 'hhmm a').format('HHmmss')
      
        appointmentDetails =  {
                            "datetime": time ,
                            "appointmentTypeID":this.props.bookingDetails.selectedService,
                            "calendarID":this.props.bookingDetails.calendarIDs[0],
                            "firstName": (this.state.user.displayName).substr(0, (this.state.user.displayName).indexOf(' ')),
                            "lastName": (this.state.user.displayName).substr((this.state.user.displayName).indexOf(' ')+1),
                            "email":this.state.user.email,
                            "phone": this.state.user.phoneNumber, 
                            "addonIDs" : this.props.bookingDetails.selectedAddons,
                            "fields" : [ { "id" : "7751812", "value" : this.props.bookingDetails.selectedAddress },  {"id" : "7881560", "value" : 50},  {"id" : "7881562", "value" : 50}  ],
                            "notes" : this.props.bookingDetails.selectedAddress,
                        }

        await fetch('https://acuityscheduling.com/api/v1/appointments?admin=true', 
                {
                    method: 'POST', 
                    headers: myHeaders, body : JSON.stringify(appointmentDetails) } ).then((response) => response.json())
        .then((responseJSON) => {  this.setOverlay(responseJSON) })
        .catch((error) => {console.log(error), this.setState({errorOverlayVisible: true})})

      }

      setOverlay(responseJSON){
          console.log(responseJSON)
          if(responseJSON.status_code){
              this.setState({errorOverlayVisible: true})
          }
          else if(responseJSON.confirmationPage){
            this.setState({successOverlayVisible: true})
          }
      }

      showForm(){
        if(!this.state.selectedIndex)
              return this.showConfirmationScreen(false)
        else
            return this.showConfirmationScreen(true)
            
                //     {/* <View style={{justifyContent: 'center', alignItems: 'center'}}>
                //         <Text style={{fontSize: 30}} > Payment Method </Text>
                //     </View> */}
                    
                //     {/* <View style={{flexDirection: 'row'}}>
                //         <Input
                //         containerStyle={{width: 150}}
                //         placeholder='First Name'
                //         />
                //         <Input
                //         containerStyle={{width: 150}}
                //         placeholder='Last Name'
                //         />
                //     </View>
                //   <Input
                //     containerStyle={{width: 300}}
                //     placeholder='Credit Card Number'
                //     />
                // <View style={{flexDirection: 'row'}}>
                //     <Input
                //     containerStyle={{width: 75}}
                //     placeholder='MM'
                //     />
                //     <Input
                //     containerStyle={{width: 75}}
                //     placeholder='YY'
                //     />
                //     <Input
                //     containerStyle={{width: 75}}
                //     placeholder='CVV'
                //     />
                //     </View>
                //     <View style={{flexDirection: 'row', justifyContent: 'flex-end', alignItems:"flex-end"}}>
                //         <Button onPress={() => this.props.togglePaymentScreen()} titleStyle={{color: 'black'}} type={{type: 'outline'}} style={{paddingTop: 30, width: 80, alignSelf: 'flex-end', }} title="Cancel" />
                //         <Button buttonStyle={{backgroundColor: 'black',}} style={{paddingTop: 30, width: 80, alignSelf: 'flex-end'}} title="Submit" />
                //     </View> */}
            
          
      }

    displayAddons(){
        
        
            return (
                this.props.bookingDetails.addonNames.map( (item) => {
                    return (<Text numberOfLines={1} style={{alignSelf: 'flex-start', color: 'white', padding: 3, fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto', }}> {item} </Text>)
                }
                )
            )
    }
    displayAddonsIfExists(){
        if(this.props.bookingDetails.addonNames.length > 0){
            return (
                <View>
                            {/* <Text> {this.displayAddons()} </Text> */}
                            <Tooltip  containerStyle={{width: 200, height: 40*this.props.bookingDetails.addonNames.length}}withOverlay={false} backgroundColor={'black'} heigth={200} popover={  this.displayAddons()  }>
                                <Text  style={{ paddingLeft: 20, fontSize: 16 , fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto', color: 'teal'}}>  +{this.props.bookingDetails.addonNames.length} Addons  </Text>
                            </Tooltip>
                </View>
            )
        }
    }
                        

    showConfirmationScreen(now){
   
        return <View style={{ height: Dimensions.get('window').height}}>
                    
                    <Card title="Booking Details">
                    
                    {/* <View style={{paddingTop: 30, justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={{fontSize: 30, fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto'}}> Booking Details </Text>
                    </View> */}
                    <View style={{paddingTop: 15}}>
                        <Text style={{fontSize: 18, fontWeight: 'bold', fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto'}}> {moment(this.props.bookingDetails.chosenDateObject).format('LL')}   </Text>
                        <Text style={{fontSize: 16 , fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto'}}>  {this.props.bookingDetails.formattedTime} </Text>
                    </View>
                    <View style={{paddingTop: 15}} >
                        <Text style={{fontSize: 18 ,fontWeight: 'bold', fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto' }}>{this.props.bookingDetails.selectedServiceName} </Text>
                        {this.displayAddonsIfExists()}
                    </View>
                    <View style={{paddingTop: 15}}>
                        <Text style={{fontSize: 18 ,fontWeight: 'bold', fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto' }}> Duration: </Text>
                        <Text style={{paddingLeft: 20, fontSize: 16 , fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto'}}> {this.props.bookingDetails.addonDuration + this.props.bookingDetails.serviceDuration} minutes </Text>
                    </View>
                    <View style={{paddingTop: 15}}>
                        <Text style={{fontSize: 18 ,fontWeight: 'bold', fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto' }}> Donation Total: </Text>
                        <Text style={{paddingLeft: 20, fontSize: 16 , fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto'}}> ${this.props.bookingDetails.servicePrice + this.props.bookingDetails.addonPrices + this.props.bookingDetails.tip}  </Text>
                    </View>
                    <View style={{paddingTop: 20}}>
                        <Text style={{fontSize: 18 ,fontWeight: 'bold',  fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto' }}> Location: </Text>
                        <Text style={{ paddingLeft: 20, fontSize: 16 , fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto'}}> {this.props.bookingDetails.selectedAddress} </Text>
                    </View>
                 
                    </Card>
                    
                    {this.ShowConfirmButton(now)}
                    {/* {this.showPaymentOptions(now)} */}
                    
            </View>
    }
    ShowConfirmButton(now){
       // if(now)
             return <Button onPress={() => this.createBooking()} type="outline" buttonStyle={{ borderColor: 'black',backgroundColor:'black', padding: 13, paddingLeft: 50, paddingRight: 50,   margin: 20,borderRadius: 1, }} titleStyle={{color: 'white'}} style={{  paddingTop: 10,    alignSelf: 'center'}} title="Confirm" />

    }
    showPaymentOptions(now){
        if(!now)
             return <View style={{margin: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}> 

                        <TouchableOpacity style={{justifyContent: 'center', alignItems: 'center'}} onPress={() => this.onStartCardEntry()} style={{  }}>
                        <Icon style={{alignSelf: 'center'}} name="credit-card" color='#262525' size={50} solid />
                        <Text style={{fontSize: 18 ,fontWeight: 'bold',  fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto' , alignSelf: 'center'}}> Pay  </Text>
                        </TouchableOpacity>
                        </View>

    }

    successOverlay(){
        this.props.showIntro(true)
     
        
        return <Overlay isVisible={this.state.successOverlayVisible}
                        width="auto"
                        height="auto">
                    <View style={{justifyContent: 'center', alignItems: 'center', padding:10}}>
                    <Icon name="check-circle" size={70} />
                    <Text style={{ marginTop: 30,  fontSize: 30, fontWeight: '100', fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto' }}>Booking Complete</Text>
                    <Text style={{ marginTop: 30,  width: 300,  fontSize: 17, fontWeight: '100', fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto' }}>Thank you, your appointment has been scheduled. A confirmation email has been sent to: { '\n\n' + this.state.user.email }.    </Text>
                    <Text style={{ marginTop: 30,  width: 300,  fontSize: 13, color: 'red', fontWeight: '100', fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto'}}>  Please Note: Your Donation can be given in cash after Service is complete (as it's cash only at this time.)   </Text>


                    <Button onPress={() => this.props.home()} type="outline" buttonStyle={{ alignSelf: 'flex-end' , borderColor: 'black',backgroundColor:'black', marginTop: 33, paddingLeft: 30, paddingRight: 30,   borderRadius: 1, }} titleStyle={{color: 'white'}} style={{  paddingTop: 10,    alignSelf: 'flex-end'}} title="OK" />
                    </View>
                </Overlay>
    }

    render() {
        const buttons = ['Pay now', 'Pay later']
        const { selectedIndex } = this.state
        return (
            <View style={styles.container}>
                   {this.state.successOverlayVisible ? this.successOverlay() : null}
                    <Overlay isVisible={this.state.errorOverlayVisible}
                            width="auto"
                            height="auto">
                        <View style={{justifyContent: 'center', alignItems: 'center', padding:10}}>
                        <Icon name="times-circle" size={70} />
                        <Text style={{ marginTop: 30,  fontSize: 30, fontWeight: '100', fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto' }}> Oops</Text>
                        <Text style={{ marginTop: 30,  width: 300,  fontSize: 17, fontWeight: '100', fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto' }}> {this.state.cardError ? 'There was a problem with your card. Please try again.' : 'There was a problem with your booking. Please try again, or contact: 6604-555-5555'}</Text>
                        <Button onPress={() => this.setState({errorOverlayVisible : false})} type="outline" buttonStyle={{ alignSelf: 'flex-end' , borderColor: 'black',backgroundColor:'black', marginTop: 33, paddingLeft: 30, paddingRight: 30,   borderRadius: 1, }} titleStyle={{color: 'white'}} style={{  paddingTop: 10,    alignSelf: 'flex-end'}} title="OK" />
                        </View>
                    </Overlay>
           
                    <View style={{flex: 1, alignSelf: 'flex-start'}}>
                        <TouchableOpacity onPress={() => this.props.togglePaymentScreen()}style={{paddingLeft: 10, paddingTop: 10, }}>
                            <Icon name="arrow-left" size={25} color="#424141" />
                        </TouchableOpacity>
                    </View>

                    {/* <View style={{flex: 4}}>
                    <ButtonGroup
                        onPress={this.updateIndex}
                        selectedIndex={selectedIndex}
                        buttons={buttons}
                        containerBorderRadius={0}
                        selectedButtonStyle={{backgroundColor: 'black'}}
                        containerStyle={{height: 30, width: 200, }}
                        />
                    </View> */}

                    <View style={{flex: 1}}>
                    </View>
        

            {this.showForm()}
            
            </View>
        );
    }
}

const styles = StyleSheet.create(
    {
        container:{

            paddingBottom: 30,
            paddingTop: 10,
            justifyContent: 'center',
            alignItems: 'center'
            

        },
    })

export default PaymentScreen;
