import React, { Component } from 'react';
import {Platform, StyleSheet, Text,Image, View,TouchableOpacity, StatusBar, Dimensions} from 'react-native';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import moment from "moment";

class CalendarSelect extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            upcomingDatesObject: {},

         };
        
    }
 
    formatDate(date){
        return new Date(date.year, date.month-1, date.day)
    }

formatUpcomingDates(){
    var upcomingDates = this.props.upcomingDates
    var upcomingDatesObject = {}
    upcomingDates.map(
        (days) => {
            upcomingDatesObject[days] = {selected: false, marked: true, }
        }
    )
    this.setState({upcomingDatesObject})
}
 
componentDidMount(){
    this.formatUpcomingDates()
}
    render() {
       // today = moment(new Date).format('YYYY-MM-DD')
        return (
<CalendarList
style = {styles.container}
monthsCount={1}
//horizontal={true}
scrollEnabled={false}
theme={{calendarBackground: '#212121', selectedDayTextColor: 'black', selectedDotColor: 'black',textSectionTitleColor: '#ffffff', dayTextColor: '#ffffff' , monthTextColor: 'white', textDayFontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto',
textMonthFontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto',
textDayHeaderFontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto',
arrowColor: 'white',
'stylesheet.calendar.header': {
    header: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingLeft: 10,
      paddingRight: 10,
      marginTop: 6,
      alignItems: 'center'
    }
  }
 }}


   minDate={this.props.upcomingDates[0]}
   maxDate={Array.from(this.props.upcomingDates).pop() }
  markedDates={this.state.upcomingDatesObject}
   onDayPress={(day) => { this.props.setChosenDate(this.formatDate(day)); this.props.scrollToIndex(this.formatDate(day)); this.props.hide('calendar')}}
    monthFormat={'MMMM'}
    hideArrows={false}
   hideExtraDays={true}
   disableMonthChange={true}
   firstDay={1}
  hideDayNames={false}

  showWeekNumbers={false}

/>
 
        );
    }

}

export default CalendarSelect;

const styles = StyleSheet.create({

    container : {
        //height: Dimensions.get('window').height/2, 
        height: 375,
        backgroundColor: "#212121",
        
   
    }
}
)