import React, { Component } from 'react';
import { Text, View, StyleSheet,Keyboard, Dimensions, TextInput, TouchableOpacity , Alert} from 'react-native';
import MapView, { Marker } from 'react-native-maps';
//import Icon from 'react-native-vector-icons/FontAwesome';
import { Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialIcons'
import ViewShot from 'react-native-view-shot'

const extraCharge = ['Richmond','Delta','Surrey','West Vancouver','North Vancouver']
const localCities = ['Vancouver','Burnaby','New Westminster','Coquitlam','Port Coquitlam', 'Port Moody' ]
const britishColumbia = 'British Columbia'
const defaultCoordinates = {
                latitude: 49.2768684,
                longitude: -123.1319551,
                latitudeDelta: 0.0122,
                longitudeDelta: 0.0121,
}

class MapSelect extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            loaded : false,
            address: defaultCoordinates,
            selectedAddress : '',
            formattedAddress: '',
         };
    }

    componentDidMount(){
        this.setState({loaded: true})
    }

    fetchAddressbyCoordinates(coordinates){
       
        fetch('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + coordinates.latitude + ',' + coordinates.longitude + '&key=AIzaSyBx5dWF2JCdlgs35UPcJ1-ukQuM2jpwGng')
        .then((response) => response.json()).then((responseJSON) => { this.setCityandProvinceName(responseJSON.results[0].address_components) ; this.setState({selectedAddress : responseJSON.results[0].formatted_address}) ;this.setFormattedAddress((responseJSON.results[0].formatted_address))})
        .catch((error) => {
            this.alert("Could not find Address. Please try again")
            this.resetMap(defaultCoordinates)
            this.setState({formattedAddress : ""})
        })
        
    }

    resetMap(address){
        this.setState({address})
    }

    setFormattedAddress(formattedAddress){
        
        secondComma =  formattedAddress.lastIndexOf(",");
        //console.log( )
        if(this.state.selectedProvince === britishColumbia && localCities.includes(this.state.selectedCity))
        {
            this.setState({formattedAddress:  formattedAddress.substr(0, secondComma)})
            this.props.addOutofZoneCharge(false)
        }
            
        else if(this.state.selectedProvince === britishColumbia && extraCharge.includes(this.state.selectedCity))
            {
                this.setState({formattedAddress : formattedAddress.substr(0, secondComma) })
                this.alert('A travel charge will be added to your total')
                this.props.addOutofZoneCharge(true)
            }
        else
        {
            this.resetMap(defaultCoordinates)
            this.alert('This City is outside the current service zone')
            this.setState({formattedAddress : ""})
            this.props.addOutofZoneCharge(true)
        }

    }

    setCityandProvinceName(addressComponents){
        addressComponents.forEach((component) => {
            if(component.types[0] == 'locality')
                this.setState({selectedCity : component.long_name })
            if(component.types[0] == 'administrative_area_level_1')
                this.setState({selectedProvince : component.long_name })
        })
        
    }

    fetchAddressbyName(address){
        console.log(address)
        address = address.replace(/\s/g, "+")
        fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&components=country:CA&key=AIzaSyBx5dWF2JCdlgs35UPcJ1-ukQuM2jpwGng')
        .then((response) => response.json()).then((responseJSON) => { this.setState({selectedAddress : responseJSON.results[0].formatted_address}) ; this.setCityandProvinceName(responseJSON.results[0].address_components) ; this.setCoordinates(responseJSON.results[0].geometry.location) })
        .catch((error) => {
            
            this.alert("Could not find Address. Please try again")
            this.resetMap(defaultCoordinates)   
            this.setState({formattedAddress : ""})
        })
    }

    setCoordinates(coordinates){
        
        var address = {
            latitude: coordinates.lat,
            longitude: coordinates.lng,
            latitudeDelta: 0.0122,
            longitudeDelta: 0.0121,
        }
    
        if(localCities.includes(this.state.selectedCity) && this.state.selectedProvince === britishColumbia){
            this.setState({address})
            this.props.addOutofZoneCharge(false)
        }
            
        else if(this.state.selectedProvince === britishColumbia && extraCharge.includes(this.state.selectedCity))
        {
            this.setState({address})
            this.alert('A travel charge will be added to your total')
            this.props.addOutofZoneCharge(true)
        }
            
        else
        {
            this.resetMap(defaultCoordinates)
            this.alert('This City is outside the current service zone, or the address was not found')
            this.setState({formattedAddress : ""})
            this.props.addOutofZoneCharge(false)
        }
        
    }

    alert(message){
        Alert.alert(
            '',
            message,
            [
              {text: 'OK'},
            ],
            { cancelable: false }
          )
    }

    setDraggedToCoordinates(coordinates){
        
        var address = {
            latitude: coordinates.latitude,
            longitude: coordinates.longitude,
            latitudeDelta: 0.0122,
            longitudeDelta: 0.0121,
        }
        this.setState({address})
    }

    saveThumbnail = async function (){
        if(this.state.selectedAddress){
            secondComma =  this.state.selectedAddress.lastIndexOf(",");
            var mapThumb = await this.refs.viewShot.capture()
            this.props.setMapDetails(mapThumb, (this.state.selectedAddress).substr(0, secondComma))
            this.props.hide("map")
        }
        
    }

    render() {
        return (
        <View style={styles.container}>
            <ViewShot style={styles.map} ref="viewShot" options={{ format: "jpg", quality: 0.9 }}>
                    <MapView
                        style={styles.map}
                        initialRegion={this.state.address}
                        region={this.state.address} >
                        <Marker draggable
                            onDragEnd={(e) =>  {this.setDraggedToCoordinates( e.nativeEvent.coordinate); this.fetchAddressbyCoordinates(e.nativeEvent.coordinate) }}
                            coordinate={this.state.address}
                             />
                    </MapView>
            </ViewShot>
          
        <View style={styles.textInputView}> 
        <Input
            value = {this.state.formattedAddress}
            onChangeText = {(formattedAddress) => this.setState({formattedAddress})}
            placeholder='Full address with Postal Code'
            placeholderTextColor={'#888888'}
            inputContainerStyle={{borderBottomWidth: 0}}
            onBlur={(object) =>  this.fetchAddressbyName(this.state.formattedAddress)   }
            rightIcon={
                <TouchableOpacity onPress= {() => { Keyboard.dismiss()} } >
                    <Icon size={23} type='ion-icon' name='search'  />
                </TouchableOpacity>
            }
            />
   
        </View>
        
        <View style={styles.mapButtons}>
                    <TouchableOpacity style={styles.cancelButton} onPress={() => this.props.hide("map")} >
                        <Text style={styles.text}> Cancel </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.okButton} onPress={() =>  {this.saveThumbnail();  } } >
                        <Text style={styles.text}> OK </Text>
                    </TouchableOpacity>
        </View>
     
    </View>
        );
    }
}

export default MapSelect;


const styles = StyleSheet.create({
    container: {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      justifyContent: 'flex-end',
      alignItems: 'center',
      //backgroundColor: 'white',
    },
    map: {
      position: 'absolute',
      top: 40,
      left: 0,
      right: 0,
      bottom: 0,
    },
    mapButtons : {
        flexDirection: 'row', 
        justifyContent: 'center',
        alignItems: 'center',
        position:'absolute', 
        //bottom: Dimensions.get('window').height/6,
        width: Dimensions.get('window').width , 
        backgroundColor: 'white',
        paddingBottom: 50,
        paddingTop: 30
        
    },
    okButton:  {
        padding: 10, 
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        width: 100,
        borderColor: 'black',
        borderBottomRightRadius: 6, 
        borderTopRightRadius: 6,
    },
    cancelButton: 
    { 
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10, 
        borderWidth: 1, 
        borderColor: 'black',
        width: 100, 
       borderBottomLeftRadius: 6, 
      borderTopLeftRadius: 6,
    },
    textInputView: {
        flexDirection: 'row', 
        justifyContent: 'center',
        alignItems: 'center',
        position:'absolute', 
        top: Dimensions.get('window').height/6,
        width: Dimensions.get('window').width/1.15 , 
        backgroundColor: 'white',
        borderRadius: 30,
        borderWidth: StyleSheet.hairlineWidth

    },
    textInput: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        borderBottomWidth: 0,
    },
    textInputIcon: {

    }
  });
