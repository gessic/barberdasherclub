import React, { Component } from 'react';
import { StyleSheet, ScrollView, FlatList, Dimensions, View, Text, Platform, TouchableOpacity} from 'react-native';
import moment from "moment";

class DaySelect extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            selectedDate : '2019-09-23',
            index: 0,

         };
    }

   _renderDay(item){
     
       return(
        <TouchableOpacity style={styles.textGroup}>
            <Text style={styles.text} > {moment(item).format('ddd').toUpperCase()} </Text>
            <Text style={styles.text}> {moment(item).format('DD')} </Text>
        </TouchableOpacity> 
       )
   }

   shouldComponentUpdate(){

    
        if(this.props.index > 0){
            this.scrollToIndex(this.props.index)
        }
       
       return true
   }

   scrollToIndex = (index) => {
    this.flatListRef.scrollToIndex({animated: true, index});
  }

    render() {
        return (
            <View  style={styles.container} >
              <FlatList
                horizontal={true}
                ref={(ref) => { this.flatListRef = ref; }}
                data={this.props.upcomingDates}
                showsHorizontalScrollIndicator={false}
                renderItem={({ item }) => this._renderDay(item)} 
                keyExtractor={item => item.id}
                initialScrollIndex={0}
                
                getItemLayout={(data, index) => (
                    {length: Dimensions.get('window').width/7 , offset: Dimensions.get('window').width/7 * index, index}
                  )}
            />

            </View>
        );
    }
}

export default DaySelect;

const styles = StyleSheet.create(
    {
        container:{
            paddingLeft: 30,
            paddingRight: 30,
            //justifyContent: 'center',
            flex: 1,
            flexDirection: 'row',
        },
        text: {
            fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto',
            fontSize: 16,
        },
     
        textGroup: {
            justifyContent: 'center',
            alignItems: 'center',
            width: Dimensions.get('window').width/7,
            
        },
      
    }
);
