import React, { Component } from 'react';
import {Platform, StyleSheet, Text,Image, Animated,  View,TouchableOpacity, StatusBar, Dimensions, FlatList, TouchableWithoutFeedback} from 'react-native';
import ServiceSelect from './ServiceSelect';
import DateSelect from './DateSelect';
//import DaySelect from './DaySelect';
//import TimeSelect from './TimeSelect';
import TipSelect from './TipSelect';
import PaySelect from './PaySelect';
import AddonSelect from './AddonSelect';
import CalendarSelect from './CalendarSelect';
import MapSelect from './MapSelect';
import MapView, { Marker } from 'react-native-maps';
import PaymentScreen from './PaymentScreen';
import TipScreen from './TipScreen';
import Icon from 'react-native-vector-icons/FontAwesome5';

//how far in advance can we book?

const base64 = require('base-64');
const hours = ['9:00','9:30', '10:00' ,'10:30' , '11:00' ,'11:30' ,'12:00' ,'12:30' ,'1:00' ,'1:30' ,'2:00' ,'2:30' ,'3:00' ,'3:30' ,'4:00' ,'4:30' ,'5:00' ,'5:30' ,'6:00' ,'6:30' ,'7:00' ,'7:30' , '8:00', '8:30']
const amHours = [ '9:00' ,'9:30' ,'10:00' ,'10:30' ,'11:00' ,'11:30' , ]
const afterHours = [ '9:00' ,'9:30' ,'10:00' , '6:00' ,'6:30' ,'7:00' ,'7:30' , '8:00', '8:30']

const moment = require('moment-timezone');
class Booking extends Component {

    constructor(props) {
        super(props);
        this.state = { 
            serviceSelect : true,
            mapRegion: '',
            selectedAddress: '',
            selectedService: this.props.hasServiceBeenSelected ? this.props.service.id : '' , 
            selectedServiceAddonIDs: [],
            selectedDate: '',
            selectedTime: '',
            formattedTime: '',
            selectedAddons: [  ],
            total: 0,
            tip: 0,
            categories: [],
            addOnShow: false,
            calendarShow: false,
            mapShow: false,
            paymentShow: false,
            showTipScreen: false,
            chosenDateObject: new Date(),
            openings: [],
            showAddressError: false,
            showServiceError: false,
            showDateError: false,
            showTimeError: false,
            disableButtons: false,
            addonPrices: 0,
            servicePrice: 0,
            selectedServiceName: '',
            addonNames: [],
            serviceDuration: 0,
            addonDuration: 0,
            calendarID: '3130477',
            calendarIDs: [],
            afterHoursCharge: 30,
            isAfterHours: false,
            addOutofZoneCharge: false,
            selectedCategory: this.props.hasServiceBeenSelected ? this.props.service.category : '',
         };

         this.bounceAnimation = new Animated.ValueXY({ x: 0, y: 0 })
         this.addOutofZoneCharge = this.addOutofZoneCharge.bind(this)
         this.display = this.display.bind(this)
         this.hide = this.hide.bind(this)
         this.setChosenDate = this.setChosenDate.bind(this)
         this.scrollToIndex = this.scrollToIndex.bind(this)
         this.updateAddons = this.updateAddons.bind(this)
         this.setSelectedService = this.setSelectedService.bind(this)
         this.calculatePrice = this.calculatePrice.bind(this)
         this.setTip = this.setTip.bind(this)
         this.setMapDetails = this.setMapDetails.bind(this)
         this.showPaymentScreen = this.showPaymentScreen.bind(this)
         this.togglePaymentScreen = this.togglePaymentScreen.bind(this)
         this.toggleTipScreen = this.toggleTipScreen.bind(this)
         this._bounce = this._bounce.bind(this)
         service = this.props.service
         
    }

    componentDidMount() {
        var myHeaders = new Headers();
        myHeaders.append('Authorization', "Basic " + base64.encode("18061059:df533abcef6b28a54a80a6e53737a935"));
        var cdate =  moment(this.state.chosenDateObject).format('YYYY-MM-DD')
        //get all calendars
        if(this.props.hasServiceBeenSelected){
            this.display('addon')
            this.setSelectedService(true, service.id, parseFloat(service.price), service.name, service.duration, service.calendarIDs)  
            
        }
        fetch('https://acuityscheduling.com/api/v1/availability/times?appointmentTypeID=10889568&calendarID=' + this.state.calendarID + '&date=' + cdate, {headers: myHeaders} ).then((response) => response.json()).then((responseJSON) => { this.setstate(responseJSON) }).catch((error) => {console.log(error)})

    }

    // setCategories = async function(){
    //     apptTypes = this.props.apptTypes;
    //     categoriesTemp = []
    //     await apptTypes.map((item) => {
    //         categoriesTemp.push(item.category)
    //     });
    //     categories = await new Set(categoriesTemp)
    //     categories = Array.from(categories)
    //     return await categories
    // }

    _bounce = () => {
        this.bounceAnimation.setValue({ x: 0, y: 0 }),
        Animated.spring(
            this.bounceAnimation, {
            toValue: {x: 0, y: -20},
            friction: 0,
          }).start()
    }


    updateAddons(selectedAddons, addonPrices, addonNames, addonDuration){
        this.setState({selectedAddons, addonPrices, addonNames, addonDuration})
        this.hide('addon')
    }

    setTip(tip){
        this.setState({tip})
    }

    getAvailability =  async function(date, serviceID, calendarID){
        var myHeaders = new Headers();
        myHeaders.append('Authorization', "Basic " + base64.encode("18061059:df533abcef6b28a54a80a6e53737a935"));
        var cdate =  moment(date).format('YYYY-MM-DD')
   
       
       await fetch('https://acuityscheduling.com/api/v1/availability/times?appointmentTypeID=' + serviceID + '&calendarID='+ calendarID + '&date=' + cdate+'&timezone=America/Los_Angeles', {headers: myHeaders} ).then((response) => response.json()).then((responseJSON) => {  this.setstate(responseJSON) }).catch((error) => {console.log(error)})
      
    }
      
    setstate(avail){

            this.state.openings = []

            
           /// console.log(avail)
           if(avail){
           avail.map(
               (timeSlot) => {
                   
                   
                   item = moment(timeSlot.time).tz('America/Los_Angeles').format('h:mm')
                   this.state.openings.push(item)
                   
               }
           )
            }
            
           this.setState({hasChanged: true})
        }

    display(screen){
        
        switch(screen){
            case 'addon':
                this.setState({addOnShow: true, calendarShow: false, disableButtons: true})
            break;

            case 'calendar':
                this.setState({addOnShow: false, calendarShow: true, disableButtons: true})
            break;
            case 'map':
                this.setState({addOnShow: false, calendarShow: false, mapShow: true})
            break;
            default: 
                this.setState({addOnShow: false, calendarShow: false, mapShow: false, disableButtons: false, showTipScreen: false})
                break;
        }
        }

    setChosenDate = async function (date){
    
        newDate = moment(date)
        await this.setState({chosenDateObject: newDate})
        await this.getAvailability(newDate, this.state.selectedService, this.state.calendarIDs[0])
    }

    hide(screen){
        
        switch(screen){
            case 'addon':
                this.setState({addOnShow: false, disableButtons: false})
            break;
            case 'calendar':
                this.setState({calendarShow: false, disableButtons: false})
            break;
            case 'map':
                this.setState({mapShow: false, disableButtons: false})
            break;
        }
    }

    showAddon(){
        if(this.state.addOnShow)
            return <AddonSelect updateAddons={this.updateAddons}apptTypes={this.props.apptTypes} hide={this.hide} addons={this.props.addons} selectedService={this.state.selectedService} />
    }
    showCalendar(){
        if(this.state.calendarShow)
            return <CalendarSelect setChosenDate={this.setChosenDate} hide={this.hide} upcomingDates={this.props.upcomingDates} scrollToIndex={this.scrollToIndex} />
    }
    showTime(){
        return  <TimeSelect availability={this.state.c} timeSelection =  {this.selectTime} />
    }


    _renderDay(item){
    
        if(item === moment(this.state.chosenDateObject).format('YYYY-MM-DD')){
            return(
            <TouchableOpacity disabled={this.state.disableButtons} style={styles.individualDaySelected} onPress={() => {this.setChosenDate(new Date(item))  } }> 
              <Text style={styles.text} > {moment(item).format('ddd').toUpperCase()} </Text>
              <Text style={styles.text}> {moment(item).format('DD')} </Text>
            </TouchableOpacity> 
            )
        }
        else{
         return(
          <TouchableOpacity disabled={this.state.disableButtons} style={styles.individualDay} onPress={() => {this.setChosenDate(item), this.setState({selectedTime: ''})  }} > 
              <Text style={styles.text} > {moment(item).format('ddd').toUpperCase()} </Text>
              <Text style={styles.text}> {moment(item).format('DD')} </Text>
          </TouchableOpacity> 
         )
        }
     }
    
     _renderTime(item){
   
         var thisTime = item.toString()
         
        if(this.state.selectedService)
         {
                if(this.state.selectedTime === thisTime){
                   
                    if(amHours.includes(thisTime)){
                      
                        if(this.state.openings.includes(thisTime))
                        {
                            return(
                                <TouchableOpacity disabled={this.state.disableButtons} style={styles.individualHourSelected} onPress={ () => {this.setState({selectedTime : thisTime})} } >
                                    <Text style={this.state.openings.includes(thisTime) ? styles.textSelected : styles.textSelected} > {thisTime} </Text>
                                    <Text style={this.state.openings.includes(thisTime) ? styles.textSelected : styles.textSelected} > AM </Text>
                                </TouchableOpacity> 
                        )}
                    }
                    else{
                    if(this.state.openings.includes(thisTime))
                    {
                        return(
                            <TouchableOpacity disabled={this.state.disableButtons} style={styles.individualHourSelected} onPress={ () => {this.setState({selectedTime : thisTime})}} >
                                <Text style={this.state.openings.includes(thisTime) ? styles.textSelected : styles.textSelected} > {item.toString()} </Text>
                                <Text style={ this.state.openings.includes(thisTime) ? styles.textSelected : styles.textSelected} > PM </Text>
                            </TouchableOpacity> 
                    )}
                }
                }

                else{
                    chosenDay = (new Date(this.state.chosenDateObject)).getDay()
                    if(amHours.includes(thisTime)){
                        if(this.state.openings.includes(thisTime))
                        {
                            return(
                                <TouchableOpacity disabled={this.state.disableButtons} style={styles.individualHour} onPress={ () => { this.state.openings.includes(thisTime) ? this.setState({selectedTime : thisTime, formattedTime : thisTime + ':00 AM'}) : null , afterHours.includes(thisTime) || chosenDay === 0 || chosenDay === 1 ? this.setAfterHours() : this.setState({isAfterHours: false}) } }>
                                    <Text style={[this.state.openings.includes(thisTime) ? styles.textAvailable : styles.text1, afterHours.includes(thisTime) || chosenDay === 0 || chosenDay === 1  ? {color: 'red'} : null ]} > {thisTime} </Text>
                                    <Text style={[this.state.openings.includes(thisTime) ? styles.textAvailable : styles.text1, afterHours.includes(thisTime) || chosenDay === 0 || chosenDay === 1  ? {color: 'red'} : null ]} > AM </Text>
                                </TouchableOpacity> 
                        )}
                    }
                    else{
                      
                        if(this.state.openings.includes(thisTime))
                        {
                            
                            return(
                                <TouchableOpacity disabled={this.state.disableButtons} style={styles.individualHour} onPress={ () => { this.state.openings.includes(thisTime) ? this.setState({selectedTime : thisTime, formattedTime : thisTime + ':00 PM'}) : null , afterHours.includes(thisTime) || chosenDay === 0 || chosenDay === 1 ? this.setAfterHours() : this.setState({isAfterHours: false}) } }>
                                    <Text style={[this.state.openings.includes(thisTime) ? styles.textAvailable : styles.text1, afterHours.includes(thisTime) || chosenDay === 0 || chosenDay === 1  ? {color: 'red'} : null ]} > {item.toString()} </Text>
                                    <Text style={[this.state.openings.includes(thisTime) ? styles.textAvailable : styles.text1, afterHours.includes(thisTime) || chosenDay === 0 || chosenDay === 1  ? {color: 'red'} : null ]} > PM </Text>
                                </TouchableOpacity> 
                        )}
            }
            }
    }
     }

     setAfterHours(){
         this.setState({isAfterHours: true})
         alert('Outside of regular work hours an additional fee will be added')

     }

     alert(message){
        Alert.alert(
            'After Hours',
            message,
            [
              {text: 'OK'},
            ],
            { cancelable: false }
          )
    }

     scrollToIndex = (chosenDate) => {
        var formattedDate = moment(chosenDate).format('YYYY-MM-DD')
      
        var index = this.props.upcomingDates.findIndex((date) => {
            return (date === formattedDate)
        })
        
        
    this.flatListRef.scrollToIndex({animated: true, index});
   }

   showMap(){
        if(this.state.mapShow){
            return <MapSelect setMapDetails={this.setMapDetails} hide={this.hide} addOutofZoneCharge={this.addOutofZoneCharge} />
        }
   }

   setSelectedService = async function(setFlag, selectedService, servicePrice, selectedServiceName, serviceDuration, calendarIDs){
   
       if(setFlag){
           if(servicePrice <= 0){
                this.setState({  addonPrices: 0, servicePrice, selectedServiceName,  serviceDuration, selectedAddons: [], addonNames: [], calendarIDs })
                
            }
            else{
                await this.setState({selectedService, servicePrice, selectedServiceName, serviceDuration, calendarIDs})
                await this.getAvailability(this.state.chosenDateObject, selectedService, calendarIDs[0])  
               
            }
        }
        else{
          
            this.setState({selectedService : '', addonPrices: 0, servicePrice: 0, isAfterHours: false, selectedTime: '' })
        }
   }

   calculatePrice(){
       
        total =  (this.state.servicePrice + this.state.addonPrices + this.state.tip)
        if(this.state.isAfterHours)
            total += 30
        else
            {
                if(this.state.mapThumbnail && this.state.addOutofZoneCharge){
                    
                    if(this.state.selectedService)
                        total+= 30
                }
            }
        
       return total
   }

   addOutofZoneCharge(add){
       this.setState({addOutofZoneCharge : add})
   }
   
   showMapThumbnail(){
       if(this.state.mapThumbnail){
           return <Image style={{height: 60, width: 60 , borderRadius: 30, }}   source={{uri: this.state.mapThumbnail}} />
       }
        else
            return <Animated.Image style={[this.bounceAnimation.getLayout(),  ]} source={require('../../../assets/mapMarker.png')} />
   }

   setMapDetails(mapThumbnail, selectedAddress){
       this.setState({mapThumbnail, selectedAddress})
   }

   showPaymentScreen(){
       if(this.state.paymentShow  && !this.state.calendarShow){
           if(this.state.selectedService && this.state.selectedAddress && this.state.chosenDateObject && this.state.selectedTime)
                return <View  style={{backgroundColor: 'white'}}> 
                            <PaymentScreen showIntro={this.props.showIntro} user={this.props.user} profile={this.props.profile} home={() => this.props.show('home')} bookingDetails={this.state} price={this.calculatePrice()} togglePaymentScreen={this.togglePaymentScreen}/> 
                        </View>
            else{
                this.togglePaymentScreen()
                if(!this.state.selectedService )
                    this.displayServiceError()    
                if(!this.state.selectedAddress )
                    this.displayAddressError()
                if(!this.state.chosenDateObject )
                    this.displayDateError()
                if(!this.state.selectedTime)
                    this.displayTimeError()
                
            }
           
       }
                 
   }
   showTipScreen(){
       
       if(this.state.showTipScreen){
           return  <TipScreen setTip={this.setTip} toggleTipScreen={this.toggleTipScreen} total={this.state.servicePrice + this.state.addonPrices} />
       }
   }
   toggleTipScreen(){
       this.setState({showTipScreen : !this.state.showTipScreen, disableButtons: !this.state.disableButtons })
   }

   togglePaymentScreen(){  
    //paymentShow = !this.state.paymentShow
    this.setState({ paymentShow : !this.state.paymentShow})
   }

   displayServiceError(){
       //this.setState({ showServiceError: !this.state.showServiceError})
       setTimeout(() => (
        this.setState({showServiceError: !this.state.showServiceError })
    ), 1);

        setTimeout(() => (
            this.setState(previousState => (
            { showServiceError: !previousState.showServiceError }
            )) 
        ), 2000);
   }
   displayAddressError(){
    this._bounce()
    
    
}
    displayDateError(){
        setTimeout(() => (
        this.setState({showDateError: !this.state.showDateError })
    ), 1);

        setTimeout(() => (
            this.setState(previousState => (
            { showDateError: !previousState.showDateError }
            )) 
        ), 2000);
    }
    displayTimeError(){
        setTimeout(() => (
        this.setState({showTimeError: !this.state.showTimeError })
    ), 1);

        setTimeout(() => (
            this.setState(previousState => (
            { showTimeError: !previousState.showTimeError }
            )) 
        ), 2000);
    }

    timeRender(){
        if(this.state.selectedService){
            
            if(this.state.openings.length>0){
                return <FlatList
                                    horizontal={true}
                                    ref={(ref) => { this.flatListRef2 = ref; }}
                                    data={hours}
                                    showsHorizontalScrollIndicator={false}
                                    renderItem={({ item }) => this._renderTime(item)} 
                                    keyExtractor={item => item.id}
                                    initialScrollIndex={0}
                                    getItemLayout={(data, index) => (
                                        {length: Dimensions.get('window').width/10 , offset: Dimensions.get('window').width/10 * index, index}
                                    )}
                                />
            }
            else{
                return(
                    <View style={{alignSelf: 'center', opacity: .5 , paddingTop: 10}}>
                        <Text style={styles.text}> No times available for this date. </Text>
                    </View>
                )
            }
        }
        else{
            return(
                <View style={{alignSelf: 'center', opacity: .5 , paddingTop: 10}}>
                    <Text style={styles.text}> No Service has been selected. </Text>
                </View>
            )
        }
        


    }
    
    render() {
        
        if(this.state.calendarShow )
        {
            
        return (
            <TouchableWithoutFeedback onStartShouldSetResponder={() => false} onMoveShouldSetResponder={() => false} onStartShouldSetResponderCapture={() => false} onMoveShouldSetResponderCapture={() => false} onPress={() => {this.display()} }>
            <View style={{flex: 1, }} >
               
               
                <View style={styles.header} >
                    <TouchableOpacity style={ { flex: .1, paddingLeft:20,  }} onPress={() => {this.props.show('home') }} >
                        <Icon  name="bars" size={25} color='white'   />
                    </TouchableOpacity>
                    <View style={ { flex: .8, justifyContent: 'center', alignItems:'center', flexDirection: 'column' }}> 
                    <Image style={{marginTop : 19, marginRight: 10,}} source={require('../../../assets/bdcBlackWhiteIcon.png')} />
                    {/* <Text style={ {paddingTop: 15,color: 'white', fontSize: 35, fontFamily: 'OstrichSansRounded-Medium'}}> Anthony Martin </Text> */}
                    </View>
                </View>
                
                <View style={styles.serviceSelect} >
                    
                    <ServiceSelect apptTypes={this.props.apptTypes} addons={this.props.addons} selectedAddons={this.state.selectedAddons} selectedService={this.state.selectedService} selectedCategory={this.state.selectedCategory} hasServiceBeenSelected={this.props.hasServiceBeenSelected} selectedServiceName={this.state.selectedServiceName} setSelectedService={this.setSelectedService} display={this.display} showError={this.state.showServiceError} />
                </View>

                <View style={styles.divider}>
                </View>
                <View style={styles.dateSelect} >

                    <TouchableOpacity  onPress={() => this.display('map')}>
                        {this.showMapThumbnail()}
                    </TouchableOpacity>
                    <DateSelect   display={this.display} date={this.state.chosenDateObject}  />
                </View>
                <View style={[styles.daySelect, this.state.showDateError ? {borderColor: 'red', borderWidth: 1,borderRadius: 50,} : null ]} >
                    <View  style={styles.daySelectInner} >
                         <FlatList
                            horizontal={true}
                            ref={(ref) => { this.flatListRef = ref; }}
                            data={this.props.upcomingDates}
                            showsHorizontalScrollIndicator={false}
                            renderItem={({ item }) => this._renderDay(item)} 
                            keyExtractor={item => item.id}
                            initialScrollIndex={0}
                            getItemLayout={(data, index) => (
                                {length: Dimensions.get('window').width/7 , offset: Dimensions.get('window').width/7 * index, index}
                            )}
                        />

                    </View>
                   
                </View>
                <View style={styles.divider}>
                </View>
                <View style={[styles.timeSelect, this.state.showTimeError ? {borderTopColor: 'red', borderBottomColor: 'red', borderTopWidth: 1, borderBottomWidth : 1} : null ]} >
                    {this.timeRender()}
                    {/* <FlatList
                                horizontal={true}
                                ref={(ref) => { this.flatListRef2 = ref; }}
                                data={hours}
                                showsHorizontalScrollIndicator={false}
                                renderItem={({ item }) => this._renderTime(item)} 
                                keyExtractor={item => item.id}
                                initialScrollIndex={0}
                                getItemLayout={(data, index) => (
                                    {length: Dimensions.get('window').width/10 , offset: Dimensions.get('window').width/10 * index, index}
                                )}
                            /> */}
                </View>
                <View style={styles.divider}>
                </View>
                <View style={styles.tipSelect} >
                    <TipSelect disableButtons={this.state.disableButtons} toggleTipScreen={this.toggleTipScreen} setTip= {this.setTip} />
                </View>
                <View style={styles.divider}>
                </View>
                <View style={styles.paySelect} >
                    <PaySelect  disableButtons={this.state.disableButtons} togglePaymentScreen={this.togglePaymentScreen} price={this.calculatePrice} />
                </View>
                {this.showAddon()}
    
                <View style={{position: 'absolute', width: '100%', marginTop : Dimensions.get('window').height/7}}>  
                {this.showCalendar()} 
                </View> 
                <View style={{position: 'absolute', width: '100%', marginTop : Dimensions.get('window').height/7}}>
                    {this.showPaymentScreen()}
                </View>
              <View style={styles.tipScreen}>  
                {this.showTipScreen()}
              </View> 
              {this.showMap()}
              
            </View> 
            </TouchableWithoutFeedback>
            
        );}
        else{
            return (
                <View style={{flex: 1, }} >

                    <View style={styles.header} >
                        <TouchableOpacity style={ { flex: .1, paddingLeft:20,  }} onPress={() => {this.props.show('home') }} >
                            <Icon  name="bars" size={25} color='white'   />
                        </TouchableOpacity>
                        <View style={ { flex: .8, justifyContent: 'center', alignItems:'center', flexDirection: 'column' }}> 
                            <Image  style={{marginTop : 19, marginRight: 10, }} source={require('../../../assets/bdcBlackWhiteIcon.png')} />
                            {/* <Text style={ {paddingTop: 15,color: 'white', fontSize: 35, fontFamily: 'OstrichSansRounded-Medium'}}> Anthony Martin </Text> */}
                        </View>
                    </View>
                    
                    <View style={styles.serviceSelect} >
                    <ServiceSelect apptTypes={this.props.apptTypes} addons={this.props.addons} selectedCategory={this.state.selectedCategory} selectedAddons={this.state.selectedAddons} selectedService={this.state.selectedService} hasServiceBeenSelected={this.props.hasServiceBeenSelected} selectedServiceName={this.state.selectedServiceName} setSelectedService={this.setSelectedService} display={this.display} showError={this.state.showServiceError} />
                    </View>
    
                    <View style={styles.divider}>
                    </View>
                    <View style={styles.dateSelect} >
    
                        <TouchableOpacity  onPress={() => this.display('map')}>
                            {this.showMapThumbnail()}
                        </TouchableOpacity>
                        <DateSelect   display={this.display} date={this.state.chosenDateObject}  />
                    </View>
                    <View style={[styles.daySelect, this.state.showDateError ? {borderColor: 'red', borderWidth: 1,borderRadius: 50,} : null ]} >
                        <View  style={styles.daySelectInner} >
                             <FlatList
                                horizontal={true}
                                ref={(ref) => { this.flatListRef = ref; }}
                                data={this.props.upcomingDates}
                                showsHorizontalScrollIndicator={false}
                                renderItem={({ item }) => this._renderDay(item)} 
                                keyExtractor={item => item.id}
                                initialScrollIndex={0}
                                getItemLayout={(data, index) => (
                                    {length: Dimensions.get('window').width/7 , offset: Dimensions.get('window').width/7 * index, index}
                                )}
                            />
    
                        </View>
                       
                    </View>
                    <View style={styles.divider}>
                    </View>
                    <View style={[styles.timeSelect, this.state.showTimeError ? {borderTopColor: 'red', borderBottomColor: 'red', borderTopWidth: 1, borderBottomWidth : 1} : null ]} >
                        {this.timeRender()}
                        {/* <FlatList
                                    horizontal={true}
                                    ref={(ref) => { this.flatListRef2 = ref; }}
                                    data={hours}
                                    showsHorizontalScrollIndicator={false}
                                    renderItem={({ item }) => this._renderTime(item)} 
                                    keyExtractor={item => item.id}
                                    initialScrollIndex={0}
                                    getItemLayout={(data, index) => (
                                        {length: Dimensions.get('window').width/10 , offset: Dimensions.get('window').width/10 * index, index}
                                    )}
                                /> */}
                    </View>
                    <View style={styles.divider}>
                    </View>
                    <View style={styles.tipSelect} >
                        <TipSelect disableButtons={this.state.disableButtons} toggleTipScreen={this.toggleTipScreen} setTip= {this.setTip} />
                    </View>
                    <View style={styles.divider}>
                    </View>
                    <View style={styles.paySelect} >
                        <PaySelect  disableButtons={this.state.disableButtons} togglePaymentScreen={this.togglePaymentScreen} price={this.calculatePrice} />
                    </View>
                    {this.showAddon()}
        
                    <View style={{position: 'absolute', width: '100%', marginTop : Dimensions.get('window').height/7}}>  
                    {this.showCalendar()} 
                    </View> 
                    <View style={{position: 'absolute', width: '100%', marginTop : Dimensions.get('window').height/7}}>
                        {this.showPaymentScreen()}
                    </View>
                  <View style={styles.tipScreen}>  
                    {this.showTipScreen()}
                  </View> 
                  {this.showMap()}
                  
                </View> 
                
            );
        }
    }
}

export default Booking;

const styles=StyleSheet.create(
    {
        header : {
            backgroundColor : '#212121',
            flex: 1.6,
            flexDirection: 'row',
            alignItems: 'center'

        },
        serviceSelect : {
            flex: 4,

        },
        dateSelect : {
            flexDirection: 'row',
            backgroundColor: 'white',
    
        },
        daySelect : {
            
            justifyContent: 'center',
            flex: 1.0,
            backgroundColor: 'white'
        }
        ,
        timeSelect : {
            flex: .7,
            paddingTop: 5,
            paddingBottom: 5,
            backgroundColor: 'white'

            
        }
        ,
        tipSelect : {
            
            flex: .7,
            marginLeft: 20,
            marginRight: 20,
            justifyContent: 'center',

        }
        ,
        paySelect : {
            flex: .8,
            marginLeft: 20,
            marginRight: 20,
            marginBottom: 20,
            justifyContent: 'center',

        },
        divider: {
            borderBottomWidth: .5,
            borderBottomColor: "#707070",
            shadowColor: "#212121",
            shadowOffset: {
                width: 3,
                height: 1,
            },
            shadowOpacity: 0.7,
            shadowRadius: 4,
        },
        daySelectInner:{
            paddingLeft: 30,
            paddingRight: 30,
            
            flex: 1,
            flexDirection: 'row',
        },
        text: {
            fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto',
            fontSize: 16,
        },
     
        individualDay: {
            justifyContent: 'center',
            alignItems: 'center',
            width: Dimensions.get('window').width/7,
      
        },
        individualDaySelected: {
            justifyContent: 'center',
            alignItems: 'center',
            width: Dimensions.get('window').width/7,
            borderWidth: .5,
            borderColor: 'black',
            borderRadius: 10,
            margin: 9,
            shadowColor: "#212121",
            shadowOffset: {
                width: 2,
                height: 2,
            },
            shadowOpacity: 0.4,
            shadowRadius: 1,
            backgroundColor: 'white' ,
        },
        textSelected: {
            fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto',
            fontSize: 14,
            opacity: 1,
            color: 'white',
        },
        textAvailable: {
            fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto',
            fontSize: 18,
            opacity: 1
        },
        text1: {
            fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto',
            fontSize: 18,
            opacity: .3
        },
        individualHour: {
            justifyContent: 'center',
            alignItems: 'center',
            padding: 10,
        },
        individualHourSelected: {
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'black',
            fontSize: 10,
            padding: 10,
            borderRadius: 100, 
            shadowColor: "#212121",
            shadowOffset: {
                width: 2,
                height: 2,
            },
            shadowOpacity: 0.4,
            shadowRadius: 1,
            
        },
        tipScreen : {
            position: 'absolute', 
            width: '100%', 
            
            marginTop : Dimensions.get('window').height/6, 
            alignItems: 'center', 
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 6,
            },
            shadowOpacity: 0.39,
            shadowRadius: 8.30,
            elevation: 13,
        }
    }
)