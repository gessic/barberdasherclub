import React, { Component } from 'react';
import {Platform, StyleSheet, Text, View,FlatList, TouchableOpacity, ScrollView, Dimensions} from 'react-native';
import AddonSelect from './AddonSelect';



class ServiceSelect extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            isBeardSelected: false,
            isCutSelected: false,
            isLocksSelected: false,
            isShaveSelected : false,
            appointmentTypeSelected: '',
            categories: [],
            categoryName: this.props.selectedCategory,
            selectedCategory: 0,
            selectedService: this.props.selectedService ? this.props.selectedService : '',
            selectedServiceID : '',
            selectedServiceDuration: 0,
            hasServiceBeenSelected: this.props.hasServiceBeenSelected ? this.props.hasServiceBeenSelected : false, 
         };

    }
    
    componentDidMount() {
        this.setCategories()
        
        
    }

    setCategories = async function(){
        apptTypes = this.props.apptTypes;
        categoriesTemp = []
        await apptTypes.map((item) => {
            categoriesTemp.push(item.category)
        });
        categories = new Set(categoriesTemp)
        categories = Array.from(categories)
        
        await this.setState({categories, selectedCategory: this.props.selectedCategory ? categories.indexOf(this.state.categoryName) : 0})
   
    }

    select(selected, x, y){
        
        if(selected){
            return (
                <View style={styles.serviceSelected}>
                </View>
            )
        }
        else{
            return null
        }
    }

    displayAddons(){
        addons = this.props.addons
        return addons.map((item) => {
            if(this.props.selectedAddons.includes(item.id)){
                return <View style={{flex: 1, flexDirection: 'row'}}>
                            <View  style={{flex: 3}}> 
                            <Text numberOfLines={1}> {item.name} </Text>
                            </View>
                            <View  style={{flex: 1, alignItems: 'flex-end'}}>
                                <Text> {item.duration > 0 ? '+' + item.duration + 'mins' : null}  </Text>
                            </View>
                        </View>
            }
        })   
    }

    errorView(){
        if(this.props.showError){
            return <View style={{justifyContent: 'center', alignItems: 'center',}} >
                        {/* <Text style={{color: 'red', fontSize: 20, }}> Please select a service </Text> */}
                    </View> 
        }
    }

    _renderCalendars(item, index){
    
        return(
            this.displayCalendarHeader(item.index)
        )
    }
  
    _renderServices(item){
        
        
            if(item.category == this.state.categories[this.state.selectedCategory]){
                
                return <TouchableOpacity style={[styles.serviceRow, {flexDirection: 'column'}]} onPress={() => { this.state.selectedService  ?  this.setState({selectedService: ''})  : this.setState({selectedService: item.name, selectedServiceID : item.id, selectedServiceDuration: this.state.selectedServiceDuration += item.duration}) ; this.state.selectedService ? null : this.props.display("addon"); this.state.selectedService ? this.props.setSelectedService(false, '', 0, '', 0, ) : this.props.setSelectedService(true, item.id, parseFloat(item.price), item.name, item.duration, item.calendarIDs) } }>
                            <View style={{flexDirection: 'row'}}>
                                <View style={[styles.serviceSelection, { borderColor: this.props.showError ? 'red' : 'black',}]}> 
                                    {this.select(item.id == this.props.selectedService, item.name, this.state.selectedService)}
                                </View>
                                    <Text numberOfLines={1} style={[styles.serviceText, styles.shadow]}> {item.name} </Text>
                                    <Text style={styles.serviceDuration}> {item.duration} mins.</Text>
                            </View>
                            <View style={{paddingLeft: 30}} >
                                    <View>
                                        {item.id == this.props.selectedService ? this.displayAddons(item.id) : null}
                                    </View>
                                    
                                </View>
                        </TouchableOpacity>
            }
        
        
    }

    displayCalendarHeader(index){
        
            return (
                <View style={{justifyContent: 'center', alignItems: 'center', width: Dimensions.get('window').width}}> 
                     <Text numberOfLines={1} style={{padding: 10, fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto', fontSize: 22 }}> {this.state.categories[index]}</Text>
                </View>

            )
    }

    displayPagination(){
        
            return this.state.categories.map((item, index) => {
                if (this.state.selectedCategory == index){
                 
                    return (
                            <View style={styles.pageSeleceted} >
                            </View>
                    )
                }
                else{
                    
                    return (
                            <View style={styles.page} >
                            </View>
                    )
                }
            });
    }

    _onScroll = ({ nativeEvent }) => {
        // the current offset, {x: number, y: number} 
        const position = nativeEvent.contentOffset; 
        // page index 
        const index = Math.round(nativeEvent.contentOffset.x / Dimensions.get('window').width);
        this.setState({selectedCategory: index})
        if (index !== this.state.currentIndex) {
          // onPageDidChanged
        }
      };

    render() {
    
        return (
            <View style={styles.container}>
                    <View style={styles.calendarTitle}> 
                        <FlatList
                        data={this.state.categories}
                        horizontal={true}
                        pagingEnabled={true}
                        ref={(ref) => { this.flatListRef = ref; }}
                        onMomentumScrollEnd={this._onScroll}
                        showsHorizontalScrollIndicator={false}
                        renderItem={(item, index) => this._renderCalendars(item, index)}
                        keyExtractor={item => item.id}
                        initialScrollIndex={this.state.selectedCategory}
                        getItemLayout={(data, index) => (
                            {length: Dimensions.get('window').width, offset: Dimensions.get('window').width * index, index}
                          )} />
                      {/* //  {this.displayCalendarHeader()} */}
                        
                        <View style={{width: 150, alignItems: 'center', justifyContent:'center', flexDirection: 'row', }} >
                            {this.displayPagination()}
                        </View>
                    </View>
                    <View  >
                    <FlatList
                            data={this.props.apptTypes}
                            renderItem={({ item }) => this._renderServices(item)} 
                            keyExtractor={item => item.id}
                        />
                    </View> 
                                
            </View>
        );
    }
}

const styles = StyleSheet.create(
    {
        container:{
            flex: 1,
           // justifyContent: 'space-between',
        },
        calendarTitle: {
            alignItems: 'center',
            justifyContent: 'center', 
        },
        serviceRow: {
            padding: 15, 
            justifyContent: 'flex-start', 
            flexDirection: 'row',
            shadowColor: "#212121",
            shadowOffset: {
                width: 1,
                height: 1,
            },
            shadowOpacity:.5,
            shadowRadius: 10,
            elevation: 7,
        },
        serviceText: {
            fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto',
            fontSize: 15,
            flex: 6,
        },
        serviceDuration: {
            fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto',
            fontSize: 14,
            paddingLeft: 10,
            flex: 2,
        },
        serviceSelection: {
            alignSelf: 'center',
            height: 15,
            width: 15, 
            borderWidth: 1,
            borderRadius: 10,
            justifyContent: 'center',
            alignItems: 'center',
        },
        serviceSelected:{
            backgroundColor: 'black',
            padding: 4,
            borderRadius: 40

        },
        page: {
            borderColor: 'black',
            borderWidth: StyleSheet.hairlineWidth,
            borderRadius: 40,
            height: 7,
            width: 7,
            marginRight: 5, 
            marginLeft: 5,
            
        },
        pageSeleceted: {
            backgroundColor: 'black',
            borderRadius: 12 ,
            marginRight: 5, 
            marginLeft: 5,
            height: 7,
            width:7,
        
        }
    }
)
export default ServiceSelect;
