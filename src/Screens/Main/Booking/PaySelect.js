import React, { Component } from 'react';
import {View, TouchableOpacity , Text, Platform, StyleSheet,  } from 'react-native';

class PaySelect extends Component {
    constructor(props) {
        super(props);
        this.state = { 

         };
    }
    
    render() {
        return (
            <View style={styles.container}>
                <View>
                    <Text  style={styles.balance} >  ${this.props.price()} </Text>
                    <Text style={styles.taxesAndFees}> Donation Total </Text>
                </View>
                
                <TouchableOpacity disabled={this.props.disableButtons} onPress={() => this.props.togglePaymentScreen()} style={{alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={styles.text} >
                        Next
                    </Text>
                </TouchableOpacity>
            </View>  
        );
    }
}


export default PaySelect;

const styles = StyleSheet.create(
    {
        container: {
            justifyContent: 'space-between',
            flexDirection: 'row'
        },
        balance : {
            fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto',
            fontSize: 24,
        },
        taxesAndFees : {
            fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto',
            fontSize: 16,
        },
        text: {
            fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto',
            fontSize: 24,
        },

    }
);
