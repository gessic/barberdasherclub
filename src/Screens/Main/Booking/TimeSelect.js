import React, { Component } from 'react';
import { StyleSheet, ScrollView, View, Text, Platform, TouchableOpacity} from 'react-native';
import moment from "moment";
import { opaqueType } from '@babel/types';
//how far in advance can we book?

const base64 = require('base-64');
const hours = ['09', '10', '11', '12', '01', '02', '03', '04', '05', '06', '07', '08']
class TimeSelect extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            availability: [],
            chosenDate: '',
            openings : [], 
            hasChanged: false,
         };
         
    }
        isAvailable(time){
            if(this.props.availability.includes(time)){
                this.props.timeSelection(time)
                return true
            }
            else 
                return false
        }

    render() {
        return (
            <ScrollView horizontal={true}  showsHorizontalScrollIndicator={false} contentContainerStyle={styles.container} >

                 <TouchableOpacity style={styles.textGroup} onPress={() => this.isAvailable('09')}>
                    <Text style={ this.props.availability.includes('09') ? styles.textAvailable : styles.text } > 9 </Text>
                    <Text style={ this.props.availability.includes('09') ? styles.textAvailable : styles.text } > AM </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.textGroup} onPress={() => this.isAvailable('10')} >
                    <Text style={ this.props.availability.includes('10') ? styles.textAvailable : styles.text } > 10 </Text>
                    <Text style={ this.props.availability.includes('10') ? styles.textAvailable : styles.text } > AM </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.textGroup} onPress={() => this.isAvailable('11')} >
                    <Text style={ this.props.availability.includes('11') ? styles.textAvailable : styles.text } > 11 </Text>
                    <Text style={ this.props.availability.includes('11') ? styles.textAvailable : styles.text }> AM </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.textGroup} onPress={() => this.isAvailable('12')} >
                    <Text style={ this.props.availability.includes('12') ? styles.textAvailable : styles.text } > 12 </Text>
                    <Text style={ this.props.availability.includes('12') ? styles.textAvailable : styles.text }> PM </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.textGroup} onPress={() => this.isAvailable('01')} >
               
                    <Text style={ this.props.availability.includes('01') ? styles.textAvailable : styles.text } > 1 </Text>
                    <Text style={ this.props.availability.includes('01') ? styles.textAvailable : styles.text }> PM </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.textGroup} onPress={() => this.isAvailable('02')} >
                    <Text style={ this.props.availability.includes('02') ? styles.textAvailable : styles.text } > 2 </Text>
                    <Text style={ this.props.availability.includes('02') ? styles.textAvailable : styles.text }> PM </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.textGroup} onPress={() => this.isAvailable('03')} >
                    <Text style={ this.props.availability.includes('03') ? styles.textAvailable : styles.text } > 3 </Text>
                    <Text style={ this.props.availability.includes('03') ? styles.textAvailable : styles.text }> PM </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.textGroup} onPress={() => this.isAvailable('04')} >
                    <Text style={ this.props.availability.includes('04') ? styles.textAvailable : styles.text } > 4 </Text>
                    <Text style={ this.props.availability.includes('04') ? styles.textAvailable : styles.text }> PM </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.textGroup} onPress={() => this.isAvailable('05')} >
                    <Text style={ this.props.availability.includes('05') ? styles.textAvailable : styles.text } > 5 </Text>
                    <Text style={ this.props.availability.includes('05') ? styles.textAvailable : styles.text }> PM </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.textGroup} onPress={() => this.isAvailable('06')} >
                    <Text style={ this.props.availability.includes('06') ? styles.textAvailable : styles.text } > 6 </Text>
                    <Text style={ this.props.availability.includes('06') ? styles.textAvailable : styles.text }> PM </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.textGroup} onPress={() => this.isAvailable('07')} >
                    <Text style={ this.props.availability.includes('07') ? styles.textAvailable : styles.text } > 7 </Text>
                    <Text style={ this.props.availability.includes('07') ? styles.textAvailable : styles.text }> PM </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.textGroup} onPress={() => this.isAvailable('08')}>
                    <Text  style={ this.props.availability.includes('08') ? styles.textAvailable : styles.text }> 8 </Text>
                    <Text  style={ this.props.availability.includes('08') ? styles.textAvailable : styles.text }> PM </Text>
                </TouchableOpacity>

            </ScrollView>
        );
    }
}

export default TimeSelect;

const styles = StyleSheet.create(
    {
        container:{
          
            //justifyContent: 'center',
            
            
        },
        text: {
            fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto',
            fontSize: 18,
            opacity: .3
        },
        textAvailable: {
            fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto',
            fontSize: 18,
            opacity: 1
        },
        textGroup: {
            justifyContent: 'center',
            alignItems: 'center',
            padding: 10,
        },
    }
);