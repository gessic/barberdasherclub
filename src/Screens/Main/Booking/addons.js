export const cutAddons = [ { name: 'Beard Trim', price:19, extraTime : '15' } , 
                    { name: 'E-Shave', price:10, extraTime : '15' },
                    { name: 'Esspresso', price:3.5 },
                    { name: 'Father and Son', price:35, extraTime : '30' },  
                    { name: 'Gold Series Grooming Serum', price:40 },
                    { name: 'Hair Wash', price:10, extraTime : '5' },
                    { name: 'Hot Towel Shave', price:55, extraTime : '45' },
                    { name: 'Hot Towel with Special Blend Face Toner', price:10, extraTime : '5' },
                    { name: 'Locks Maintenanace', price:68 },
                    { name: 'Mini Facial /PH Balanced Face Steam', price:25, extraTime : '5' },
                    { name: 'Refill Gold Series Grooming Serum', price:25 }
                ]

export const beardAddons = [ { name: 'Esspresso', price:3.5 },
                { name: 'Gold Series Grooming Serum', price:40 },
                { name: 'Line Up', price:10, extraTime : 10 }, 
                { name: 'Locks Maintenanace', price:68 },
                { name: 'Hot Towel with Special Blend Face Toner', price:10, extraTime : '5' },
                { name: 'Mini Facial /PH Balanced Face Steam', price:25, extraTime : '5' },
                { name: 'Quick trim around the ears, neck and fringe', price:25, extraTime : '15' },
                { name: 'Refill Gold Series Grooming Serum', price:25 }
            ]

export const shaveAddons = [ { name: 'Esspresso', price:3.5 },
                { name: 'Gold Series Grooming Serum', price:40 },
                { name: 'Line Up', price:10, extraTime : 10 }, 
                { name: 'Locks Maintenanace', price:68 },
                { name: 'Mini Facial /PH Balanced Face Steam', price:25, extraTime : '5' },
                { name: 'Refill Gold Series Grooming Serum', price:25 }
            ]

export const locksAddons = [ { name: 'Beard Trim', price:19, extraTime : '15' } , 
                { name: 'E-Shave', price:10, extraTime : 15 },
                { name: 'Esspresso', price:3.5 },
                { name: 'Fade', price:58, extraTime : 50 },  
                { name: 'Gold Series Grooming Serum', price:40 },
                { name: 'Hot Towel with Special Blend Face Toner', price:10, extraTime : '5' },
                { name: 'Mini Facial /PH Balanced Face Steam', price:25, extraTime : '5' },
                { name: 'Refill Gold Series Grooming Serum', price:25 }
            ]