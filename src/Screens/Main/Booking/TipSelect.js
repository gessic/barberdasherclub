import React, { Component } from 'react';
import {View, TouchableOpacity , Text, Platform, StyleSheet,  } from 'react-native';

class TipSelect extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            selected : {
                ten : false,
                fifteen :false,
                twenty: false,
                twentyFive : false,
                cash: false,
                custom: false, 
            }
            
         };
    }

    outline(amount, key){
        
        selected = this.state.selected
        if(this.state.selected[key]){
            this.props.setTip(0)
            
            selected[key] = false 
            this.setState({selected })
        }
        else{
            this.props.setTip(amount)
            for (var keys in selected) {
                selected[keys] = false
              }
            selected[key] = true 
            this.setState({selected })
            if(key == 'custom')
                this.props.toggleTipScreen()
        }
    }


    render() {
        return (
            <View style={styles.container}>
                <View style={{justifyContent: 'center', alignItems:'center'}}>
                    <Text style={styles.tip}> Complements </Text>      
                </View>        
                <TouchableOpacity  disabled={this.props.disableButtons} onPress={() => this.outline(10, 'ten')}  style={this.state.selected.ten ? styles.outline : null} >
                    <Text style={styles.text}> $10 </Text>  
                </TouchableOpacity>

                <TouchableOpacity  disabled={this.props.disableButtons} onPress={() => this.outline(15, 'fifteen')}  style={this.state.selected.fifteen ? styles.outline : null} >
                   <Text style={styles.text}> $15 </Text>  
                </TouchableOpacity>

                <TouchableOpacity  disabled={this.props.disableButtons} onPress={() => this.outline(20, 'twenty')}  style={this.state.selected.twenty ? styles.outline : null} >
                  <Text style={styles.text}> $20 </Text>  
                </TouchableOpacity>

                <TouchableOpacity  disabled={this.props.disableButtons} onPress={() => this.outline(25, 'twentyFive')}  style={this.state.selected.twentyFive ? styles.outline : null} >
                  <Text style={styles.text}> $25 </Text>  
                </TouchableOpacity>


                <TouchableOpacity  disabled={this.props.disableButtons} onPress={() => {this.outline(0, 'custom')}}  style={this.state.selected.custom ? styles.outline : null} >
                  <Text style={styles.text}> Custom </Text>  
                </TouchableOpacity>   
            </View>
        );
    }
}

export default TipSelect;

const styles = StyleSheet.create(
    {
        container:{

            justifyContent: 'space-evenly',
            flexDirection: 'row',
            
        },
        text: {
            fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto',
            fontSize: 16,
            opacity: .7,
        },
        tip: {
            fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto',
            fontSize: 16,
            alignSelf: 'center'

        },
        textGroup: {
            justifyContent: 'center',
            alignItems: 'center',
            padding: 10,
        },
        outline : {
            borderWidth: 1,
        }
    }
);