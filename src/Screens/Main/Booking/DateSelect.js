import React, { Component } from 'react';
import {Platform, StyleSheet, Text,Image, View,TouchableOpacity, StatusBar, Dimensions} from 'react-native';
import moment from "moment";
import Icon from 'react-native-vector-icons/FontAwesome5';

const base64 = require('base-64');
const Days = ["Sunday", "Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
const Months = ["January","February","March","April","May","June","July", "August", "September", "October", "November", "December"];

class DateSelect extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            loading: true,
            date : '',
            availability: '',
         };
    }

  
    getDayName(day){
        return this.Days[day]
    }

    setDate(date){
       newDate = moment(date).format('dddd MMMM D, YYYY')
        return newDate
    }

    render() {
       
        return (
            <View style={styles.container}>

                <View style={{ flex: 1, flexDirection: 'row',justifyContent: 'center', alignItems: 'center', paddingRight: 70,  }}>
                    <TouchableOpacity style={{flexDirection: "row",   alignItems: 'center', justifyContent: 'space-around'}} onPress={() => this.props.display('calendar')}>
                            <Icon style={{opacity: .7}} name="angle-double-left"  />
                            <Text style={{ paddingRight: 20, paddingLeft: 20,  fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto',}}>
                                {this.setDate(this.props.date)}
                            </Text>
                            <Icon style={{opacity: .7}} name="angle-double-right" />
                    </TouchableOpacity>

                </View>

            </View>
            
        );
    }
}

const styles = StyleSheet.create(
    {
        container : {
            flex : 1,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center'
        },

    }
)

export default DateSelect;