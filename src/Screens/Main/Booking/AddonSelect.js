import React, { Component } from 'react';
import { StyleSheet, ScrollView, View, Text, Platform, TouchableOpacity, FlatList, Dimensions} from 'react-native';
import {cutAddons, beardAddons, shaveAddons, locksAddons}   from './addons'

class AddonSelect extends  Component {
    constructor(props) {
        super(props);
        
        this.state = { 
            selectedAddons : [],
            chosenAddons: [],
            addonPrices: 0,
            addonNames: [],
            addonDuration: 0
         };

    }

    componentDidMount(){
        apptTypes = this.props.apptTypes
        selectedAddons = []
        apptTypes.map((item) => {
            if(item.id == this.props.selectedService){
                selectedAddons = item.addonIDs
            }
        })
       this.setState({selectedAddons})

    }

    addonSelected(index){
         if(this.state.chosenAddons.includes(index)){
            return ( <View style={styles.selected}> 
                    </View>
            )
        }
    }

    setAddonSelected(index, price, addonName, addonDuration){
        
        if(this.state.chosenAddons.includes(index)){
            //remove the actual selected addons
            chosenAddons = this.state.chosenAddons
            addonNames = this.state.addonNames
            addonNames.splice(addonNames.indexOf(addonNames), 1)
            chosenAddons.splice( chosenAddons.indexOf(index), 1 );
            if(this.state.addonPrices > 0)
                addonPrices = parseFloat(price) - this.state.addonPrices
            this.setState({chosenAddons, addonPrices, addonDuration : addonDuration -= this.state.addonDuration})
        }
        else{ 
            //add addons
            chosenAddons = this.state.chosenAddons
            addonNames = this.state.addonNames
            addonNames.push(addonName)
            chosenAddons.push(index)
            addonPrices = parseFloat(price) + this.state.addonPrices
            this.setState({chosenAddons, addonPrices, addonNames, addonDuration : addonDuration += this.state.addonDuration })
        }
    }
    

    _renderItem(item, index){
        addons = this.props.addons
      
        return addons.map((addon, index) => {
            
            if(addon.id == item){
               
                return (
                            <TouchableOpacity style={styles.addonRow} onPress={() => { this.setAddonSelected(addon.id, addon.price, addon.name, addon.duration)}} >
                                    <View style={{flexDirection: 'row', }}>
                                        <View style={styles.serviceSelection}> 
                                           {this.addonSelected(addon.id)}
                                        </View>
                                        <View> 
                                            <Text numberOfLines={2} style={styles.text}> {addon.name} </Text>
                                        </View>
                                    </View>
                                   
                                    <View>
                                    </View>
                                    <View style={styles.priceBox}>
                                        <Text> ${addon.price} </Text>
                                    </View>
                            </TouchableOpacity>
            
                    )
            }
        })

    }

    render() {
        
        return (
            <View style={styles.container}>
                <View addOnSelectText={styles.addOnSelectText} >
                    <Text style={styles.title}> Select Add-Ons </Text>
                </View>
              {/* // {this.getAddonList(this.props.selectedService)} */}
             
                 <FlatList
                    data={this.state.selectedAddons}
                    renderItem={({item, index}) => this._renderItem(item, index)}
                    keyExtractor={item => item.id}
                    /> 
                    
                <View style={{flexDirection: 'row', paddingBottom: 20, marginTop: 20 }}>
                    <TouchableOpacity style={{ padding: 10, borderWidth: 1, borderColor: 'white', borderBottomLeftRadius: 6, borderTopLeftRadius: 6 }} onPress={() => this.props.hide("addon")} >
                        <Text style={styles.text}> Cancel </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.updateAddons(this.state.chosenAddons, this.state.addonPrices, this.state.addonNames, this.state.addonDuration)} style={{padding: 10, borderWidth: 1, borderColor: 'white', borderBottomRightRadius: 6, borderTopRightRadius: 6,}} >
                        <Text style={styles.text}> OK </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

export default AddonSelect;

const styles = StyleSheet.create(
    {
        container: {
            height: Dimensions.get('window').height/1.5 , 
            width: '100%',
            backgroundColor: "#212121",
            position: 'absolute',
            opacity: .95,
            marginTop: 100,
            alignItems: 'center',

        },
        addOnSelectText : {
            alignItems: 'center',
            justifyContent: 'center',

        },
        title: {
            fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto',
            fontSize: 22,
            opacity: 1,
            color: 'white',
            alignSelf: 'center' ,
        },
        text: {
            fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto',
            fontSize: 12,
            opacity: 1,
            color: 'white',
            alignSelf: 'center' ,
        },
        priceBox: {
            backgroundColor: 'white',
            borderRadius: 15, 
            padding: 1,
            justifyContent: 'center',
            alignItems: 'center'

        },
        serviceSelection: {
            alignSelf: 'center',
            borderColor: 'white',
            height: 15,
            width: 15, 
            borderWidth: 1,
            borderRadius: 5,
            justifyContent: 'center',
            alignItems: 'center',
        },
        selected:{
            backgroundColor: 'white',
            padding: 4,
            borderRadius: 3,

        },
        addonRow : {
            flexDirection: 'row',
            paddingTop: 10,
            justifyContent: 'space-between'
        }
    }
)