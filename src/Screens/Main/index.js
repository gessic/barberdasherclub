import React, {Component} from 'react';
import {Platform, StyleSheet,StatusBar, Text,Image, View,TouchableOpacity} from 'react-native';
import Home from './Home';
import NavigationDrawer from './NavigationDrawer';
import Booking from './Booking';
import Services from './Services';
import moment from "moment";

const base64 = require('base-64');

export default class Main extends Component{
    constructor(props){
        super(props)
        this.state = {
            showIntro : false,
            homePage: true,
            navigationDrawer: false,
            bookingPage: false,
            servicesPage: false,
            formattedDates: [],
            categories : [],
            hasServiceBeenSelected: false,
            service: {},

        }
        this.show = this.show.bind(this)
        this.getRangeOfDates = this.getRangeOfDates.bind(this)
        this.setIntroVideo = this.setIntroVideo.bind(this)
        this.showIntroVideo = this.setIntroVideo.bind(this)
    }

    componentDidMount(){
        
        const today = moment();
        const end = moment().add(2, 'months')
        const dates = this.getRangeOfDates(today, end, 'days');
        formattedDates = []
        dates.map((day) => {
            formattedDates.push(day.format('YYYY-MM-DD'))
        })
        this.setState({formattedDates})
    }
    
    setIntroVideo(showIntro){
        this.state.showIntro  = showIntro
    }
    showIntroVideo(showIntro){
        this.setState({showIntro})
    }
    
    getRangeOfDates(start, end, key, arr = [start.startOf(key)]) {
  
        if(start.isAfter(end)) throw new Error('start must precede end')
        
        const next = moment(start).add(1, key).startOf(key);
        if(next.isAfter(end, key)) return arr;
        return this.getRangeOfDates(next, end, key, arr.concat(next));
        
      }
    show(page,hasServiceBeenSelected, service){
        
        if(page == 'home'){
            this.setState({ homePage: true, navigationDrawer: false, bookingPage: false})
        }
        if(page == 'navigationDrawer'){
            this.setState({ homePage: false, navigationDrawer: true, bookingPage: false })
        }
        if(page == 'booking'){

            this.setState({ homePage: false, navigationDrawer: false, bookingPage: true, hasServiceBeenSelected, service })
        }
        if(page == 'services')
            {
                this.setState({ homePage: false, navigationDrawer: false, bookingPage: false, servicesPage: true,})
            }
    }

    renderPage(){
        
        if(this.state.homePage){
            return (
             
                <Home  showIntro={this.state.showIntro} setIntroVideo={this.showIntroVideo} show={this.show} />
                
            )
        }
        else if(this.state.navigationDrawer){
            return(
                <View style={{flexDirection: 'column'}}>
                    <NavigationDrawer show={this.show}/>
                    <Home show={this.show}/>
                </View>
            )
        }
        else if(this.state.bookingPage){
            return(
                <Booking addons={this.props.addons} apptTypes={this.props.apptTypes} user={this.props.user} profile={this.props.profile} show={this.show} upcomingDates={this.state.formattedDates} hasServiceBeenSelected={this.state.hasServiceBeenSelected} service={this.state.service} showIntro={this.setIntroVideo} />
            )
        }
        else if(this.state.servicesPage){
                return (
                    <Services show={this.show} apptTypes={this.props.apptTypes}/>
                    )
        }
    }

  
    render(){
        
        return(
            this.renderPage()
            
        )
    }     
}

const styles = StyleSheet.create(
    {
        container: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          },
        bookNowButton: {
            borderWidth : 1,
            borderColor: '#212121',
            borderRadius:5,
          },
    }
)