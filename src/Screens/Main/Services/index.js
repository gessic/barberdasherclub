import React, { Component } from 'react';
import { Text, StyleSheet, View , Image, FlatList, StatusBar, TouchableOpacity, Dimensions} from 'react-native';
import { Card, Button} from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome5';


class Services extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            categories: [],
            selectedCategory: 0,
         };
    }

    componentDidMount(){
        this.setCategories()
    }

    setCategories = async function(){
        apptTypes = this.props.apptTypes;
        categoriesTemp = []
        await apptTypes.map((item) => {
            
            categoriesTemp.push(item.category)
        });
        categories = new Set(categoriesTemp)
        categories = Array.from(categories)
        await this.setState({categories})
   
    }
    _renderCalendars(item, index){
       
        return(
            this.displayCalendarHeader(item.index)
            
        )
    }
    displayCalendarHeader(index){
        return (
            <View style={{justifyContent: 'center', alignItems: 'center', width: Dimensions.get('window').width}}> 
                 <Text numberOfLines={1} style={{padding: 10, fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto', fontSize: 22 }}>   {this.state.categories[index]}</Text>
            </View>

        )
}
    _onScroll = ({ nativeEvent }) => {
        // the current offset, {x: number, y: number} 
        
        const position = nativeEvent.contentOffset; 
        // page index 
        const index = Math.round(nativeEvent.contentOffset.x / Dimensions.get('window').width);
        this.setState({selectedCategory: index})
        if (index !== this.state.currentIndex) {
        // onPageDidChanged
        }
    };

    displayPagination(){
        
        return this.state.categories.map((item, index) => {
            
            if (this.state.selectedCategory == index){
    
                return (
                        <View style={styles.pageSeleceted} >
                        </View>
                )
            }
            else{
                return (
                        <View style={styles.page} >
                        </View>
                )
            }
            
        }

        )
    }

    _renderServiceCard(item){

            if(item.category == this.state.categories[this.state.selectedCategory]){
                return (
                    <Card containerStyle={styles.cardContainer} title={item.name} image={{uri: item.image }} >
                        
                        <Text style={{marginBottom: 10, padding: 10}}>
                            {item.description}
                        </Text>
                        
                        <Button
                            buttonStyle={{backgroundColor: 'black', borderRadius: 0, marginLeft: 40, marginRight: 40, marginBottom: 0}}
                            title='Book Now'
                            onPress={() => this.props.show('booking',true,  item)} />
                    </Card>
                )
            }
    }

    render() {
        return (
            <View style={styles.container} >

               
                <View style={styles.header} >
                    <TouchableOpacity style={ { flex: .1, paddingLeft:20,  }} onPress={() => {this.props.show('home') }} >
                        <Icon  name="bars" size={25} color='white'   />
                    </TouchableOpacity>
                    <View style={ { flex: .8, justifyContent: 'center', paddingTop: 5, alignItems:'center', flexDirection: 'column' }}> 
                        <Image  source={require('../../../assets/bdcBlackWhiteIcon.png')} />
                    </View>
                </View>

                <View style={styles.calendarTitle}> 
                        <FlatList
                        data={this.state.categories}
                        horizontal={true}
                        pagingEnabled={true}
                        ref={(ref) => { this.flatListRef = ref; }}
                        onMomentumScrollEnd={this._onScroll}
                        showsHorizontalScrollIndicator={false}
                        renderItem={(item, index) => this._renderCalendars(item, index)}
                        keyExtractor={item => item.id} />
                      {/* //  {this.displayCalendarHeader()} */} 
                        <View style={{width: 150, alignItems: 'center', justifyContent:'center', flexDirection: 'row', }} >
                            {this.displayPagination()}
                        </View>
                    </View>

                <View style={styles.services}>
                    <FlatList
                                data={this.props.apptTypes}
                                renderItem={({ item }) => this._renderServiceCard(item)} 
                                keyExtractor={item => item.id}
                            />
                </View>
                

            </View>
        );
    }
}

styles = StyleSheet.create(
    {
        container: {
            flex: 1,
        },
        header : {
            backgroundColor : '#212121',
            flex: 1.5,
            flexDirection: 'row',
            alignItems: 'center',


        },
        services : {
            flex: 8,
            flexDirection: 'row',
            alignItems: 'center'

        },
        calendarTitle: {
            alignItems: 'center',
            paddingBottom: 10,
            justifyContent: 'center', 
        },
        page: {
            borderColor: 'black',
            borderWidth: StyleSheet.hairlineWidth,
            borderRadius: 40,
            height: 7,
            width: 7,
            marginRight: 5, 
            marginLeft: 5,
            
        },
        pageSeleceted: {
            backgroundColor: 'black',
            borderRadius: 12 ,
            marginRight: 5, 
            marginLeft: 5,
            height: 7,
            width:7,
        
        },
        cardContainer:{
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 5,
            },
            shadowOpacity: 0.36,
            shadowRadius: 6.68,
            borderWidth: 0,
            
            elevation: 11,
                    }
    }
)

export default Services;