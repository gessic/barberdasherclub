import React, { Component } from 'react';
import {Platform, StyleSheet,StatusBar, Dimensions, Alert, Text,Image,Easing, View,TouchableOpacity, Animated} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
const bdcIntro = require('../../assets/bdcIntro.gif')
const bdcLogo = require('../../assets/bdc.png')

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            showIntro: props.showIntro
         };
        this.logoFadeIn = new Animated.Value(0)
        this.titleFadeIn = new Animated.Value(0)
        this.bookNowButtonFadeIn = new Animated.Value(0)
        this.fadeAnim = new Animated.Value(1)
        
    }


    componentDidMount(){
        
        Animated.timing(
            this.logoFadeIn,
            {
              toValue: 1,
              duration: 500,
            }).start()

        Animated.timing(
            this.titleFadeIn,
            {
                toValue: 1,
                //delay: 500,
                duration: 500,
              },
          ).start();

          Animated.timing(
            this.bookNowButtonFadeIn,
            {
                toValue: 1,
                //delay: 700,
                duration: 500,
              },
          ).start();
    }

    introFadeIn(){
        Animated.timing(
            this.fadeAnim,
            {
              toValue: .0,
              delay:0,
              duration: 3000,
            }).start(() => { this.setState({showIntro: false}); this.props.setIntroVideo(false)  })
        }

    checkConxn(bookNow){
        fetch('https://www.google.com')
        .then(() =>  { bookNow ? this.props.show('booking') : this.props.show('services')} )
        .catch((error) => {
            Alert.alert(
                'Internet Connection Required',
                '',
                [
                  {text: 'OK'},
                ],
                { cancelable: true }
              )
        })
       
    }

    render() {
        
        if(!this.state.showIntro){
        return (
            <View >
                
                {/* <TouchableOpacity  onPress={() => this.props.show('navigationDrawer')} >
                    <Image source={require('../../assets/menu.png')} />
                </TouchableOpacity> */}
                <TouchableOpacity style={styles.menuButton}  >
                    {/* <Animated.View style={{opacity: this.bookNowButtonFadeIn}}>
                        <Icon  name="bars" size={25}   />
                    </Animated.View> */}
                </TouchableOpacity>
                
                <View style={{justifyContent: 'center', alignItems: 'center',}} >
                    <View style={{ paddingBottom: 20, flexDirection: 'row', height: Dimensions.get('window').height/2.5  }}>
                        <Animated.Image
                        source={bdcLogo}
                        style={{   alignSelf: 'flex-end', opacity: this.fadeAnimB}}
                        />
                        <Animated.Text style={{opacity: this.fadeAnimB , alignSelf: 'flex-end', paddingBottom: 10, fontSize: 13}}> 
                            TM
                        </Animated.Text>
                    </View>
                   
                    <View style={{justifyContent: 'center', alignItems: 'center'}}>
                        <Animated.Text style={{opacity: this.fadeAnimText,  fontSize: 45, paddingTop: 10,  fontWeight: '500' ,  paddingBottom: 10 , fontFamily: 'OstrichSansRounded-Medium',  letterSpacing: 5}}>  BARBERDASHERClub </Animated.Text>
                        <Animated.Text style={{opacity: this.fadeAnimText,fontSize: 13, paddingBottom: 5 , fontWeight: '100' }}>  ASSOCIATION </Animated.Text>
                        <Animated.Text style={{opacity: this.fadeAnimText,fontSize: 15 , fontWeight: '100' }}>  UN-INCORPORATED </Animated.Text>
                    </View>
                    <View style={{padding: 0, }}>
                        <TouchableOpacity onPress={() => this.checkConxn(false)} >
                        <Animated.View style={[styles.servicesButton, {opacity: this.bookNowButtonFadeIn}]} >
                            <Text style={{padding: 5, fontSize: 17, color: 'black' }}> View Services </Text>
                        </Animated.View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.checkConxn(true)} >
                        <Animated.View style={[styles.bookNowButton, {opacity: this.bookNowButtonFadeIn}]} >
                            <Text style={{padding: 5, fontSize: 17, color: 'white' }}> Book Now </Text>
                        </Animated.View>
                        </TouchableOpacity>
                        </View>
                </View>
            </View>
        );
                }
                else
                    return <View style={{justifyContent: 'center', alignItems: 'center',  flex:1, backgroundColor: 'black',}} > 
                                <Animated.Image
                                source={bdcIntro}
                                onLoad={() => this.introFadeIn()}
                                style={{backgroundColor: '#212121', width: Dimensions.get('window').width ,opacity: this.fadeAnim }}
                                />
                                <Text style={{paddingTop: 80, opacity: .7,  fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto', color: 'white', fontSize: 25}}>
                                    See you soon...
                                </Text>
                          </View>
    }
}

export default Home;

const styles = StyleSheet.create(
    {
        container: {
            flex: 1,
            margin: 10,
          },
        bookNowButton: {
            borderWidth : 1,
            borderColor: '#212121',
            borderRadius:5,
            justifyContent: 'center',
            alignItems: 'center',
            width: 150,
            marginTop: 20,
            backgroundColor: 'black',
            shadowColor: "#000",
          },
          servicesButton: {
            borderWidth : 1,
            borderColor: '#212121',
            borderRadius:5,
            justifyContent: 'center',
            alignItems: 'center',
            width: 150,
            marginTop: 30,
            backgroundColor: 'white',
            shadowColor: "#000",
          },
        menuButton: {
            margin: 10,
            marginTop: 50,
            marginLeft: 20, 
        },

    }
)
