import React, {Component} from 'react';
import {Platform, StyleSheet, KeyboardAvoidingView,Dimensions, StatusBar, Text,Image, View,TouchableOpacity, TextInput} from 'react-native';
import { TextInputMask } from 'react-native-masked-text'
import firebase from 'react-native-firebase'
import { Button, Input } from 'react-native-elements';
const base64 = require('base-64');

export default class SignUp extends Component{

    constructor(props){
        super(props)
        this.state = {
            phoneInputText: '',
            emailInputText: '',
            name:'',
            lastName: '',
            isEmailValid: false,
            isPhoneValid: '',
            isNameEmpty: '',
            nameError: false,
            lastNameError: false,
            emailError: false,
            phoneError: false

        }
    }

    validate = (text) => {
        
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
        if(reg.test(text) === false)
        {
        this.setState({emailInputText:text, isEmailValid: false})
        return false;
          }
        else {
          this.setState({emailInputText:text, isEmailValid: true})
        }
        }

    validateData(){
        if(!this.state.name){
            this.setState({nameError: true})
        }
        else
            this.setState({nameError: false})

        
        if(!this.state.lastName){
            this.setState({lastNameError: true})
        }
        else
            this.setState({lastNameError: false})

        if(!this.state.isEmailValid){
            this.setState({emailError: true})
        }
        else    
            this.setState({emailError: false})
        
        if(this.state.phoneInputText.length == 14){
            this.setState({phoneError: false})
        }
        else
            this.setState({phoneError: true})

        if(this.state.isEmailValid && this.state.phoneInputText.length == 14 && this.state.name && this.state.lastName){
            firebase.auth().signInWithPhoneNumber('+1' + this.state.phoneInputText)
            .then(confirmResult => {this.props.saveConfirmResult(confirmResult) })
            .then(() => this.addUserToDatabase())
            .catch(error => console.log(error));
            this.props.setNamePhoneAndEmail((this.state.name).replace(/\s/g, "") + ' ' + (this.state.lastName).replace(/\s/g, ""), this.state.phoneInputText, this.state.emailInputText,)
         
            this.props.show('verify')
        }
    }
 

    addUserToDatabase = async function(){
        try{
            let user = {
                firstName: this.state.name,
                lastName: this.state.lastName,
                email: this.state.emailInputText,
                phone: '+1' + (this.state.phoneInputText).replace(/\D/g,''),
            }
            const firestore = firebase.firestore();
            await firestore.collection('Users').add({
                user
              });
              var myHeaders = new Headers();
              myHeaders.append('Authorization', "Basic " + base64.encode("18061059:df533abcef6b28a54a80a6e53737a935"));
              await fetch('https://acuityscheduling.com/api/v1/clients', {method: 'POST', headers: myHeaders, body : JSON.stringify(user) } ).then((response) => {response.json()}).catch((error) => {console.log(error)})
        
        }
        catch(error){
            console.log(error)
        }
        
    }
    render(){
        return(
            <KeyboardAvoidingView  styles={{justifyContent: 'flex-end',}} keyboardVerticalOffset={75} behavior="padding" enabled>

            {/* <Text style={{marginBottom:15, fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto', fontSize: 18, fontWeight: 'bold'}}> Enter your Phone and Email </Text> */}
              
              <View style={{flexDirection: 'row'}}>
                <Input
                    placeholder="First Name"
                    returnKeyType="next"
                    placeholderTextColor={'#888888'}
                    onChangeText={(name) => this.setState({name})}
                    value={this.state.name}
                    containerStyle={this.state.nameError ? styles.inputErrorStyle : styles.inputStyle}
                    autoCorrect={false}
                    />
                    <Input
                    placeholder="Last Name"
                    returnKeyType="next"
                    placeholderTextColor={'#888888'}
                    onChangeText={(lastName) => this.setState({lastName})}
                    value={this.state.lastName}
                    containerStyle={this.state.lastNameError ? styles.inputErrorStyle : styles.inputStyle}
                    autoCorrect={false}
                    />
                </View>

                 <TextInputMask
                    style={this.state.phoneError ? styles.phoneErrorStyle : styles.phoneStyle}
                    keyboardType={'number-pad'}
                    type={'custom'}
                    placeholderTextColor={'#888888'}
                    options={{
                        mask: '(999) 999 9999'}}
                    onChangeText={(phoneInputText) => this.setState({phoneInputText})}
                    value={this.state.phoneInputText}
                    autoFocus={false}
                    placeholder="Phone"
                />
              
    
                    <Input
                    placeholder="Email"
                    returnKeyType="next"
                    placeholderTextColor={'#888888'}
                    keyboardType='email-address'
                    onChangeText={(emailInputText) => this.validate(emailInputText)}
                    value={this.state.emailInputText}
                    containerStyle={this.state.emailError ? styles.emailErrorStyle : styles.emailStyle }
                    autoCorrect={false}
                    autoComplete={true}
                    />
                
                    <Button returnKeyType="submit" onPress={() => this.validateData()} title="Confirm" buttonStyle={{borderRadius: 10, height: 40, marginTop: 20, backgroundColor: 'black' }}/>
                
            </KeyboardAvoidingView>
        )
    }

}


const styles = StyleSheet.create(
    {
        inputs: {
            width: 160,
            borderRadius: 10, 
            borderColor: '#212121', 
            borderWidth: 1, 
            margin: 10,
            padding: 10,
        },
        phoneStyle: {
            borderBottomWidth: StyleSheet.hairlineWidth, 
            width: Dimensions.get('window').width/1.66,  
            margin: 10,
            height: 40,  
            marginTop: 20,  
            fontSize: 18,
            color: '#212121'
        },
        phoneErrorStyle: {
            borderBottomWidth: StyleSheet.hairlineWidth, 
            width: Dimensions.get('window').width/1.66,  
            margin: 10,
            height: 40,  
            marginTop: 20,  
            fontSize: 18 ,
            borderColor: 'red', 
            borderRadius: 10,
            borderWidth: 2,
            padding: 10,
        },
        inputStyle: {
            width: Dimensions.get('window').width/3, 
        },
        inputErrorStyle: 
        {
            borderColor: 'red', 
            borderRadius: 10,
            borderWidth: 2,
            width: Dimensions.get('window').width/3, 
        },
        lastNameErrorStyle: 
        {
            borderColor: 'red', 
            borderRadius: 10,
            borderWidth: 2,
            width: Dimensions.get('window').width/3, 
        },
        emailStyle: {
            width: Dimensions.get('window').width/1.5, 
        },
        emailErrorStyle: {
            width: Dimensions.get('window').width/1.5, 
            borderColor: 'red', 
            borderRadius: 10,
            borderWidth: 2,
        }

    }
)
