import React, {Component} from 'react';
import {Platform, StyleSheet,StatusBar, Animated,KeyboardAvoidingView, Text,Image, View,TouchableOpacity, TextInput, Dimensions} from 'react-native';
import SignUp from './SignUp'
import Login from './Login'
import Verify from './Verify';
import ProfilePic from './ProfilePic';

const bdcLogo = require('../../assets/bdc.png')
import Icon from 'react-native-vector-icons/FontAwesome';

export default class Auth extends Component{

    constructor(props){
        super(props)
        this.state = {
            phoneInputText: '',
            emailInputText: '',
            firstTime: true,
            login: false,
            signUp: false,
            verify: false,
            profilePic: false,
            name: '',
            confirmResult: '', 
            showButtons: false,   
            showBackButton: true,
        }
        
        this.fadeAnim = new Animated.Value(1)
        this.fadeAnimB = new Animated.Value(0)
        this.fadeAnimText = new Animated.Value(0)

        this.show = this.show.bind(this)
        this.setNamePhoneAndEmail = this.setNamePhoneAndEmail.bind(this)
        this.saveConfirmResult = this.saveConfirmResult.bind(this)
    }

    componentDidMount(){
        this.fadeIn()
    }

    show(page){
        if(page == 'firstTime'){
            this.setState({ firstTime: true, login: false, verify: false, profilePic: false})
        }
        if(page == 'login'){
            this.setState({ firstTime: false, signUp: false, login: true, verify: false, profilePic: false})
        }
        if(page == 'signUp'){
            this.setState({ firstTime: false, signUp: true, login: false, verify: false, profilePic: false})
        }
        if(page == 'verify'){
            this.setState({ firstTime: false, signUp: false, login: false, verify: true, profilePic: false})
        }
        if(page == 'profilePic'){
            console.log("Calling profile pic")
            this.setState({ firstTime: false, login: false, verify: false, profilePic: true })
        }
    }

    setNamePhoneAndEmail = async function (name, phoneNumber, email){
        console.log('index', name, phoneNumber, email)
        await this.setState({name: name, phoneInputText: phoneNumber, emailInputText: email})
        this.props.setProfile(name, email, phoneNumber)
       
    }

    saveConfirmResult = async function (confirmResult){
        try{
            console.log("confor," , confirmResult)
            await this.setState({confirmResult})
        }
        catch(error){
            console.log(error)
        }
        
       
    }

    showButtons(){
         if(this.state.showButtons){
            return <View style={{flexDirection: 'row', justifyContent:'center', alignItems: 'center'}}>
{/*                 
                <TouchableOpacity onPress={() => this.show('signUp')} style={[styles.loginButtons, {backgroundColor: 'black'} ]}>
                    <Text style={{color: 'white'}}> Sign Up </Text>
                </TouchableOpacity>
                 */}
                <TouchableOpacity onPress={() => this.show('login')} style={[styles.loginButtons, {backgroundColor: 'black'} ]}>
                    <Text style={{color: 'white'}}> Login </Text>
                </TouchableOpacity>
                
            </View>
        }
    }

    comp(){
        
        if(this.state.firstTime){
            return(
                <View>
                    <View style={{justifyContent: 'center', alignItems: 'center'}}>
                        <Animated.Text style={{opacity: this.fadeAnimText,  fontSize: 45, paddingTop: 10,  fontWeight: '500' ,  paddingBottom: 10 , fontFamily: 'OstrichSansRounded-Medium',  letterSpacing: 5}}>  BARBERDASHERClub </Animated.Text>
                        <Animated.Text style={{opacity: this.fadeAnimText,fontSize: 13, paddingBottom: 5 , fontWeight: '100' }}>  ASSOCIATION </Animated.Text>
                        <Animated.Text style={{opacity: this.fadeAnimText,fontSize: 15,  paddingBottom: 30 , fontWeight: '100' }}>  UN-INCORPORATED </Animated.Text>
                    </View>
                    {this.showButtons()}
                </View>
            )
        }
        else if(this.state.signUp){
            return( 
                <SignUp show={this.show} setNamePhoneAndEmail={this.setNamePhoneAndEmail} saveConfirmResult={this.saveConfirmResult}/>
            )
        }
        else if(this.state.login){
            return( 
                <Login show={this.show} setNamePhoneAndEmail={this.setNamePhoneAndEmail}  saveConfirmResult={this.saveConfirmResult}/>
            )
        }
        else if(this.state.verify){
            return(
                <Verify show={this.show} userEmail={this.state.emailInputText} userName={this.state.name} setUser={this.props.setUser} confirmResult={this.state.confirmResult}/>
            )
        }
        else if(this.state.profilePic){
            return(
            <ProfilePic setCompleteProfilePicStep={this.props.setCompleteProfilePicStep}  />
            )
        }
    }

    fadeIn(){
    
        Animated.timing(
            this.fadeAnimB,
            {
              toValue: 1,
              duration: 1500,
            }).start()

        Animated.timing(
            this.fadeAnimText,
            {
                toValue: 1,
                duration: 1500,
            }).start(() => this.setState({showButtons: true}))
    }

    showBack(){
        
        if(!this.state.firstTime){
       
            return (
                <TouchableOpacity  style={{position: 'absolute', top: 40, left: 10}} onPress={ () => { this.setState({firstTime: true}) } }>
                     <Icon style={{marginLeft: 10, }} name="chevron-left" size={25} />
                </TouchableOpacity>
            )
        }
    }
    render(){
        
        return(
    
            <KeyboardAvoidingView style={{flex:1, backgroundColor: 'white', paddingTop: 50,}} keyboardVerticalOffset={250} behavior="padding" enabled>

            {this.showBack()}
            
            <View style={{  justifyContent: 'center', alignItems:'center'}}>
                <View style={{ paddingBottom: 20, flexDirection: 'row', height: Dimensions.get('window').height/2.5  }}>
                    <Animated.Image
                    source={bdcLogo}
                    style={{   alignSelf: 'flex-end', opacity: this.fadeAnimB}}
                    />
                    <Animated.Text style={{opacity: this.fadeAnimB , alignSelf: 'flex-end', paddingBottom: 10, fontSize: 13}}> 
                        TM
                    </Animated.Text>
                </View>
                 
                {this.comp()}
            </View>
          
            </KeyboardAvoidingView>
        )

        // else
        //     return  <View style={{flex:1, backgroundColor: 'black',}} > 
        //                 <Animated.Image
        //                 source={bdcIntro}
        //                 onLoad={() => this.introFadeIn()}
        //                 style={{height:Dimensions.get('window').height, width:Dimensions.get('window').width, opacity: this.fadeAnim }}
        //                 />
        //             </View>
    }
}

const styles = StyleSheet.create(
    {
        loginButtons: {
            borderWidth : 1,
            borderColor: '#212121',
            borderRadius:5,
            height: 35,
            width: 80,
            justifyContent: 'center', 
            alignItems:'center',
            margin: 10,
            
           
        
            },
    }
)