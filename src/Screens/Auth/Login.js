import React, {Component} from 'react';
import {Platform, StyleSheet, KeyboardAvoidingView,Dimensions, Alert,StatusBar, Text,Image, View,TouchableOpacity, TextInput} from 'react-native';
import {TextInputMask} from 'react-native-masked-text';
import firebase from 'react-native-firebase'
import {Button} from 'react-native-elements';


export default class Login extends Component{

    constructor(props){
        super(props)
        this.state = {
            phoneInputText: '',
            emailInputText: '',
            isEmailValid: false,
            isPhoneValid: '',
            emailError: false,
            phoneError: false

        }
    }

    validateData(){
    try{
        if(this.state.phoneInputText.length == 14){
            this.setState({phoneError: false})
        }
        else
            this.setState({phoneError: true})


        if(this.state.phoneInputText.length == 14 ){
            firebase.auth().signInWithPhoneNumber('+1' + this.state.phoneInputText)
            .then(confirmResult => {this.props.saveConfirmResult(confirmResult) })
            .catch(error => console.log('firbease', error));
       
            try{
               // this.props.setNamePhoneAndEmail((this.state.name).replace(/\s/g, "") + ' ' + (this.state.lastName).replace(/\s/g, ""), this.state.phoneInputText, this.state.emailInputText,)
                this.props.show('verify')
            }
            catch(error){
                console.log('er1', error)
            }
            
        }
    }
    catch(error){
        console.log(error)
    }
    }

    checkUserExists = async function(){
        phoneNumber = '+1' + (this.state.phoneInputText).replace(/\D/g,'')
        try
        {
            const firestore = firebase.firestore()
            console.log(phoneNumber)
            const user = await firestore.collection('Users').get()
            // const user = await firestore.collection('Users').get()
            let exists = false
            user.docs.map(doc => {
                console.log(doc)
                if(doc._data.user.phone === phoneNumber){
                   this.props.setNamePhoneAndEmail( doc._data.user.firstName + ' ' + doc._data.user.lastName , doc._data.user.phone, doc._data.user.email)
                    exists = true 
                }
                    
            })
            if(exists){
                return true
            }
            else
                return false
        }
        catch(error){
            console.log('er2', error)
            return false
        }

    }

    validateAndConfirm = async function(){
        exists = await this.checkUserExists(this.state.phoneNumber)
        if(exists)
        {
            this.validateData()
        }
        else
        {
                Alert.alert(
                    'Access to app is for private members only. ',
                    'Email: info@barberdasherclub.com to book your first service with us!',
                    [
                      {text: 'OK'},
                    ],
                    { cancelable: false }
                  )
                  this.props.show('firstTime')
        }
    }
 
    render(){
        return(
            <KeyboardAvoidingView  styles={{justifyContent: 'flex-end',}} keyboardVerticalOffset={75} behavior="padding" enabled>

            {/* <Text style={{marginBottom:15, fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto', fontSize: 18, fontWeight: 'bold'}}> Enter your Phone and Email </Text> */}
             
                 <TextInputMask
                    style={this.state.phoneError ? styles.phoneErrorStyle : styles.phoneStyle}
                    keyboardType={'number-pad'}
                    type={'custom'}
                    options={{
                        mask: '(999) 999 9999'}}
                    onChangeText={(phoneInputText) => this.setState({phoneInputText})}
                    value={this.state.phoneInputText}
                    autoFocus={false}
                    placeholder="Phone"
                    placeholderTextColor={'#888888'}
                />
              
                    <Button returnKeyType="Next" onPress={() => this.validateAndConfirm()} title="Confirm" buttonStyle={{borderRadius: 10, height: 40, marginTop: 20, backgroundColor: 'black' }}/>
                
            </KeyboardAvoidingView>
        )
    }

}


const styles = StyleSheet.create(
    {
        inputs: {
            width: 160,
            borderRadius: 10, 
            borderColor: '#212121', 
            borderWidth: 1, 
            margin: 10,
            padding: 10,
        },
        phoneStyle: {
            borderBottomWidth: StyleSheet.hairlineWidth, 
            width: Dimensions.get('window').width/1.66,  
            margin: 10,
            height: 40,  
            marginTop: 20,  
            fontSize: 18 ,
            color: '#212121'
        },
        phoneErrorStyle: {
            borderBottomWidth: StyleSheet.hairlineWidth, 
            width: Dimensions.get('window').width/1.66,  
            margin: 10,
            height: 40,  
            marginTop: 20,  
            fontSize: 18 ,
            borderColor: 'red', 
            borderRadius: 10,
            borderWidth: 2,
            padding: 10,
        },
        inputStyle: {
            width: Dimensions.get('window').width/3, 
        },
        inputErrorStyle: 
        {
            borderColor: 'red', 
            borderRadius: 10,
            borderWidth: 2,
            width: Dimensions.get('window').width/3, 
        },
        lastNameErrorStyle: 
        {
            borderColor: 'red', 
            borderRadius: 10,
            borderWidth: 2,
            width: Dimensions.get('window').width/3, 
        },
        emailStyle: {
            width: Dimensions.get('window').width/1.5, 
        },
  emailErrorStyle: {
            width: Dimensions.get('window').width/1.5, 
            borderColor: 'red', 
            borderRadius: 10,
            borderWidth: 2,
        }

    }
)
