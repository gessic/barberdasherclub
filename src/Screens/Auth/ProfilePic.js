import React, { Component } from 'react';
//import ImagePicker from 'react-native-image-crop-picker';
import { StyleSheet, Text,Image, View,TouchableOpacity, TextInput} from 'react-native';
import ImagePicker from 'react-native-image-picker';



class ProfilePic extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            profilePicSource: '',
         };


    }

    launchImage = async function(){
        // More info on all the options is below in the API Reference... just some common use cases shown here
        const options = {
            title: 'Select Profile Picture',
            storageOptions: {
            skipBackup: true,
            path: 'images',
            },
        };
  
        /**
         * The first arg is the options object for customization (it can also be null or omitted for default options),
         * The second arg is the callback which sends object: response (more info in the API Reference)
         */
        ImagePicker.showImagePicker(options, (response) => {
          
        
            if (response.didCancel) {
            console.log('User cancelled image picker');
            } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
            } else {
            const source = { uri: response.uri };
           

            //save the URI to  Asyncstoruage here :
            

            // You can also display the image using data:
            // const source = { uri: 'data:image/jpeg;base64,' + response.data };
            this.setState({
                profilePicSource: source.uri,
            });
            }
        });
    }

    loadImage(){
        
        if(this.state.profilePicSource){
            return <Image style={{width: 200, height: 200, borderRadius:150,}}
                          source={{uri: this.state.profilePicSource }} />
        }
        else
            return <Text> Add Profile Pic </Text>
    }

    render() {
        return (
            <View>
                <TouchableOpacity style={styles.profilePic} onPress={() => this.launchImage()} >
                {this.loadImage()}
                </TouchableOpacity>
                <View style={{flexDirection: 'row', }}>
                    <TouchableOpacity onPress={() => this.props.setCompleteProfilePicStep(true)}> 
                        <Text> Skip </Text> 
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.setCompleteProfilePicStep(true)}> 
                        <Text> Done </Text> 
                </TouchableOpacity>
                </View>
                
            </View>
        );
    }
}

export default ProfilePic;

const styles = StyleSheet.create(
    {
        profilePic: {
            borderWidth : 1,
            borderColor: '#212121',
            borderRadius:150,
            height: 200,
            width: 200,
            justifyContent: 'center', 
            alignItems:'center',
            margin: 10,
        }
    }
)
