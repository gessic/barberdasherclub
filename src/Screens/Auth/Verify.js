import React, { Component } from 'react';
import {Platform, StyleSheet, Text,Image, View,TouchableOpacity, TextInput} from 'react-native';
import firebase, { firestore } from 'react-native-firebase'

class Verify extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            verificationCode : '',
            showVerificationError: '',
         };
         
    }

    verify(){
        
        if(this.props.confirmResult.confirm){
        this.props.confirmResult.confirm(this.state.verificationCode)
        .then(async (user) =>  { await user.updateEmail(this.props.userEmail.trim(), await user.updateProfile({displayName: this.props.userName }))
                                .then(() => user.sendEmailVerification())
                                .then(() => this.updateUserInDatabase(user.uid, user.phoneNumber))
                                .then(() => this.props.show('profilePic'))
                                .catch(error => console.log(error)) })
        .catch(error => {console.log(error), this.setState({showVerificationError: true})})
        }
        else
            this.setState({showVerificationError: true})
    }

    setUser(user){
        if(user){
            this.props.setUser(user)
        }
    }

    updateUserInDatabase = async function(userId, userPhone){
     
        try{
            const firestore = firebase.firestore()
            const user = await firestore.collection('Users').where('phoneNumber' , '==', userPhone).get()
            const docID = user.docs[0].id
          
            await firestore.collection('Users').doc(docID).update({'isPhoneVerified' : true, userId})
        }
        catch(error){
            console.log(error)
        }
    }
    
    render() {
        return(
            
            <View style={{flexDirection: 'column'}}>
           
                {this.state.showVerificationError ? <Text style={[styles.text, {color: 'red'}]}> Incorrect Verification Code </Text> : null}
                <View style={{flexDirection: 'row'}}>
                <TextInput
                        style={styles.inputs}
                        returnKeyType="next"
                        keyboardType="number-pad"
                        placeholderTextColor={'#888888'}
                        onChangeText={(verificationCode) => this.setState({verificationCode})}
                        value={this.state.verificationCode}
                        autoCorrect={false}
                        placeholder="Verification Code"
                    />
                <TouchableOpacity onPress={() => this.verify()}>
                       <Image
                        style={{width: 40, height: 40, margin: 10}}
                        source={require('./assets/nextCaret.png')}
                        />
                    </TouchableOpacity>
                    </View>
            </View>
        )
    }
}

export default Verify;

const styles = StyleSheet.create(
    {
        inputs: {
            height: 40, 
            width: 160,
            borderRadius: 10, 
            borderColor: '#212121', 
            borderWidth: 1, 
            margin: 10,
            padding: 10,
            color: '#212121'
            
        },
        text: {
            fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto',
            fontSize: 16,

        },
    }
)