import React, {Component} from 'react';
import {Platform, StyleSheet, KeyboardAvoidingView, Text,Image, View,TouchableOpacity} from 'react-native';



export class MenuButton extends Component{
    render(){
        return(
            <TouchableOpacity style={styles.menuButton}>
                <Image
                    style={{width: 30, height: 20}}
                    source={require('../assets/menu2.png')}
                    />
            </TouchableOpacity>
        )
    }
}
const styles = StyleSheet.create({
        menuButton:{
            position: 'absolute',
            top: 50,
            left: 30
        },
    }
)