import React, {Component} from 'react';
import { StyleSheet,} from 'react-native';
import Main from './src/Screens/Main'
import Auth from './src/Screens/Auth'
import firebase from 'react-native-firebase'
const base64 = require('base-64');

//import constnat files

//This is the parent element for the whole project. 
//It just returns either the Auth component, or the Main componnet
//It also logs the user in ¿¿
export default class App extends Component {

  constructor(props){
    super(props)
    this.state = {
      isAuthenticated: false,
      loading: true,
      user: null,
      completeProfilePicStep: false,
      currentUsername: '',
      currentUserEmail: '',
      currentPhone: '',
      appointmentTypes: [],
      addons: [],
      responseJSON: [],
      categories: [],
    }
    
    this.setCompleteProfilePicStep = this.setCompleteProfilePicStep.bind(this)
  }

  //Once the app loads, the user will be signed in automatically (unless its their first time.)
  componentDidMount() {
    console.disableYellowBox = true;

    var myHeaders = new Headers();
    myHeaders.append('Authorization', "Basic " + base64.encode("18061059:df533abcef6b28a54a80a6e53737a935"));
    fetch('https://acuityscheduling.com/api/v1/appointment-types', {headers: myHeaders} ).then((response) => response.json()).then((responseJSON) => {   this.setState({responseJSON : this.filterPrivate(responseJSON)}) }).catch((error) => {console.log(error)})
    fetch('https://acuityscheduling.com/api/v1/appointment-addons', {headers: myHeaders} ).then((response) => response.json()).then((responseJSON) => { this.setState({addons: responseJSON}); }).catch((error) => {console.log(error)})

    //Call acuity API to get the Services { }
    //Call Acuity API to get the all addons
    
    firebase.auth().onAuthStateChanged((user) => {
    
      //console.log(user)
      if (user) {
        
        user.reload()
        this.setState({ loading: false, isAuthenticated: true, user: user });
        
      } else {
        this.setState({ loading: false, isAuthenticated: false });
      }
    });
  }

  
  setProfile(currentUsername, currentUserEmail, currentPhone){
    this.setState({currentUsername, currentUserEmail, currentPhone})
  }

  setCompleteProfilePicStep(completeProfilePicStep){
    this.setState({completeProfilePicStep})
  }

  //filter out private appointment types
  filterPrivate(apptTypes){
    
    return apptTypes.filter(apptType => {
      if(!apptType.private){
        return apptType
      }
    })
  }

  render() {
    
    
    //if this.state.loading is true, the app is still loading. 
    // if(this.state.loading){
    //   return null
    // }

    //add a flag to ensure profile only shown on first step
    //if(this.state.isAuthenticated && this.state.completeProfilePicStep){
    if(this.state.isAuthenticated){
    return (

        <Main user={this.state.user} apptTypes={this.state.responseJSON} addons={this.state.addons} profile={{userName: this.state.currentUsername, email: this.state.currentUserEmail, phone: this.state.currentPhone}}/>

    );
  }
  else{
    return( 
      <Auth setProfile={this.setProfile} setCompleteProfilePicStep={this.setCompleteProfilePicStep} />
    )
  }
  }
}

const styles = StyleSheet.create({

  splash: {
    flex: 1,
    backgroundColor: 'white',
  },
 
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

});
